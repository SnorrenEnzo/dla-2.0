import numpy as np
from keras.callbacks import Callback
from keras.optimizers import Adam, RMSprop

from srcnn_helpers import outputf

import sys

class OutputObserver(Callback):
	""""
	callback to observe the output of the network
	"""

	def __init__(self, n, nn, train_example):
		self.out_log = []
		self.nn = nn
		self.n = n # number of epochs before output
		self.train_example = train_example
	
	def on_epoch_end(self, epoch, logs={}):
		if epoch % self.n == 0:
			prediction = self.nn.predict(self.train_example)
			prediction = prediction.reshape(1,100,100,1)
			self.out_log.append(prediction)
			show_image(prediction[0],Save=True,epoch=epoch)


class show_inputprediction(Callback):
	""""
	callback to observe the output of the network
	shows one input image, one true and one predicted every n epochs
	"""

	def __init__(self, n, nn, X_test, Y_test, prediction_loc, imgsize, filters_used):
		self.nn = nn # the neural network object
		self.n = n # number of epochs before output
		self.X_test = X_test
		self.Y_test = Y_test
		
		self.prediction_loc = prediction_loc
		self.imgsize = imgsize

		self.filters_used = filters_used

		self.of = outputf()

		#whether to save the images as separate, clean pngs
		self.saveclean = False


	def on_epoch_end(self, epoch, logs={}):
		#predict the small test set and save
		if epoch % self.n == 0:
			predictions = self.nn.model.predict(self.X_test)
			
			self.of.show_input_and_prediction(predictions, self.X_test, self.Y_test, epoch, self.prediction_loc, self.imgsize, self.filters_used, self.nn, saveclean = self.saveclean)


class loss_history(Callback):
	
	'''
	DESCRIPTION -----------------------------------------------------------
	
	Keras callback to get loss value after every n batches.
				
	ATTRIBUTES ------------------------------------------------------------
	
	n --> number of batches to wait before checking loss
	
	'''
		
	def __init__(self, nn, model):
		self.seen = 0
		self.nn = nn
		self.model = model

	def on_train_begin(self, logs={}):
		self.train = []
		self.test = []
		self.learning_rate_history = []

	def on_batch_end(self, batch, logs={}):		
		self.seen += logs.get('size', 0)
		if self.seen % self.nn.n_batches_loss_plot == 0:
			self.train.append(logs.get('loss'))		

	def on_epoch_end(self, batch, logs={}):		
		self.test.append(logs.get('val_loss'))