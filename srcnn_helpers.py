import numpy as np
import pandas as pd
from tensorflow.python.keras.utils import Sequence
from tqdm import tqdm
# from numdisplay_zscale import zscale
from PIL import Image
from astropy.io import fits

#for source extraction
import sep

import matplotlib
matplotlib.use('agg')
from matplotlib import pyplot as plt
#to set x ticks to integers
from matplotlib.ticker import MaxNLocator
from matplotlib.patches import Ellipse

import srcnn_utils as SU

import os

def concatenate_split_arrays(x, idx = None):
	"""
	Concatenate the four-way split arrays back for easy file saving
	"""

	if idx is None:
		return np.concatenate((x[0], x[1], x[2], x[3]), axis = -1)
	else:
		return np.concatenate((x[0][idx], x[1][idx], x[2][idx], x[3][idx]), axis = -1)


class outputf(object):

	def __init__(self):
		#how many images to use for sextractor evaluation during
		#training
		self.after_epoch_sex_images = 60

		#scale value for plotting fluxes of source extractor; prevents large numbers on the plot
		self.scale_value = 1

		#### for plotting predictions
		#range of values for the imgshow
		self.vmin = -200
		self.vmax = 2000
		#criteria for displaying sources detected with SExtractor
		self.minflux = 0.1*self.scale_value
		self.edgedist = 5

	def show_input_and_prediction(self, predictions, X_test, Y_test, epoch, prediction_loc, imgsize, filters_used, nn, epochfirst = False, saveclean = True, compact_view = True):
		"""
		Plot several input images, their respective ground truth and
		superresolution predictions.
		"""
		if compact_view:
			source_indicator_size = 20
		else:
			source_indicator_size = 10
		source_flux_fontsize = 6

		#size of title font for plotting
		fontsize = 8

		def remove_all_ticks(ax):
			ax.tick_params(left = False, right = False,
							bottom = False, top = False,
							labelbottom = False, labelleft = False)

		#concatenate the four groups in X_test to the normal shape
		X_test = concatenate_split_arrays(X_test)

		# X_test = X_test.reshape(X_test.shape[0],imgsize,imgsize,len(filters_used))
		# Y_test = Y_test.reshape(Y_test.shape[0],imgsize,imgsize)
		# predictions = predictions.reshape(predictions.shape[0],imgsize,imgsize)

		SEx = source_extraction(nn)

		for i in range(X_test.shape[0]):
			x_image = X_test[i,:,:,0] # only show the first filter
			#the target filter, but with low resolution
			x_lowres_image = X_test[i,:,:,-1]
			y_image = Y_test[i,:,:,0]
			pred_image = predictions[i,:,:,0]

			#set the savenames. If epochfirst = True, then the epoch number is
			#the first that is mentioned
			if epochfirst:
				all_imgs_name = f'Epoch={epoch+1}_img{i}_all_imgs'
				true_img_name = f'Epoch={epoch+1}_img{i}_true_image'
				pred_img_name = f'Epoch={epoch+1}_img{i}_pred_image'
			else:
				all_imgs_name = f'All_imgs_epoch={epoch+1}_img{i}'
				true_img_name = f'True_image_epoch={epoch+1}_img{i}'
				pred_img_name = f'Pred_image_epoch={epoch+1}_img{i}'


			###############################################################
			# Here, corrections for the preprocession are performed
			###############################################################
			if nn.log_images:
				#back to linear, scaled counts
				x_image = 10**x_image
				y_image = 10**y_image
				x_lowres_image = 10**x_lowres_image
				pred_image = 10**pred_image

			#rescale with scale value. No scaling is applied if log images
			#is used, as then the scale value is set to 1 in the main
			x_image = x_image * nn.scale_value
			y_image = y_image * nn.scale_value
			x_lowres_image = x_lowres_image * nn.scale_value
			pred_image = pred_image * nn.scale_value

			#load the SExtractor data of these images
			names = self.image_filenames(nn, i)


			#run SExtractor for source flux deviations
			objects_true = SEx.sep_extract(y_image, perfect = True)
			objects_pred = SEx.sep_extract(pred_image, perfect = False)

			objects_true = SEx.remove_edge_objects(objects_true, imgsize)
			objects_pred = SEx.remove_edge_objects(objects_pred, imgsize)

			#filter out all points lower than 1, as these can cause problems
			#when plotting log
			y_image[y_image < 1] = 1
			pred_image[pred_image < 1] = 1

			#scale the Y images here so it isn't done twice
			# y_image = np.log10(y_image)
			# pred_image = np.log10(pred_image)

			fig = plt.figure()

			if not compact_view:
				ax = fig.add_subplot(231)
				ax.imshow(np.log10(x_image), cmap='gray', vmin = np.log10(self.vmin), vmax = np.log10(self.vmax))
				ax.set_title(f'One of the Input images\n[log10 scaling], img {i}', fontsize=fontsize)
				remove_all_ticks(ax)

				ax = fig.add_subplot(232)
				ax.set_title(f'True image\n[log10 scaling], img {i}', fontsize=fontsize)
				ax.imshow(y_image, cmap='gray', vmin = np.log10(self.vmin), vmax = np.log10(self.vmax))
				remove_all_ticks(ax)

				ax = fig.add_subplot(234)
				ax.set_title(f'Low resolution target filter\n[log10 scaling], img {i}', fontsize=fontsize)
				ax.imshow(np.log10(x_lowres_image), cmap='gray', vmin = np.log10(self.vmin), vmax = np.log10(self.vmax))
				remove_all_ticks(ax)

				ax = fig.add_subplot(235)
				ax.set_title(f'Prediction \n[log10 scaling], img {i}', fontsize=fontsize)
				ax.imshow(pred_image,cmap='gray', vmin = np.log10(self.vmin), vmax = np.log10(self.vmax))
				remove_all_ticks(ax)


				########
				# Plot the same images, but with SExtractor source indicators
				########
				ax = fig.add_subplot(233)
				ax.set_title(f'True image, sources\n[log10 scaling], img {i}', fontsize=fontsize)
				ax.imshow(y_image, cmap='gray', vmin = np.log10(self.vmin), vmax = np.log10(self.vmax))
				remove_all_ticks(ax)
				#plot the source indicators
				ax = self.plot_source_indicators(ax, objects_true, imgsize)

				ax = fig.add_subplot(236)
				ax.set_title(f'Prediction, sources\n[log10 scaling], img {i}', fontsize=fontsize)
				ax.imshow(pred_image,cmap='gray', vmin = np.log10(self.vmin), vmax = np.log10(self.vmax))
				remove_all_ticks(ax)
				#plot the source indicators
				ax = self.plot_source_indicators(ax, objects_pred, imgsize)
			else:
				#make more compact images by plotting only the prediction and sources
				ax = fig.add_subplot(211)
				ax.set_title(f'True image\n[lin scaling], img {i}', fontsize=fontsize)
				ax.imshow(y_image, cmap='gray', vmin = self.vmin, vmax = self.vmax)
				remove_all_ticks(ax)
				#plot the source indicators
				ax = self.plot_source_indicators(ax, objects_true, imgsize)

				ax = fig.add_subplot(212)
				ax.set_title(f'Prediction\n[lin scaling], img {i}', fontsize=fontsize)
				ax.imshow(pred_image, cmap='gray', vmin = self.vmin, vmax = self.vmax)
				remove_all_ticks(ax)
				#plot the source indicators
				ax = self.plot_source_indicators(ax, objects_pred, imgsize)

			if not compact_view:
				plt.subplots_adjust(wspace = 0.1, hspace = 0.1)
			else:
				plt.subplots_adjust(hspace = 0.2)
			plt.savefig(f'{prediction_loc}{all_imgs_name}.png', dpi=300, bbox_inches='tight')
			plt.close()

			if saveclean:
				#also save the image to a clean png
				true_img = Image.fromarray(y_image).convert('RGB')
				true_img.save(f'{prediction_loc}{true_img_name}.png')

				pred_img = Image.fromarray(pred_image).convert('RGB')
				pred_img.save(f'{prediction_loc}{pred_img_name}.png')

			#run SExtractor to extract source fluxes and saving them
			#to file
			# SEx.flux_deviation_end_of_training(Y_test[i]*nn.scale_value, predictions[i]*nn.scale_value, img_id = i, epoch = epoch)

	def plot_source_indicators(self, ax, objects, imgsize = 100):
		"""
		Plot the source indicators of SExtractor for the per epoch
		image summary
		"""

		#sizes of text
		# source_indicator_size = 20
		source_flux_fontsize = 6

		# plot an ellipse for each object
		for i in range(len(objects)):
			#only plot if the flux is high enough
			if objects['flux'][i]/1e3 > 10:
				e = Ellipse(xy = (objects['x'][i], objects['y'][i]),
							width = 6*objects['a'][i],
							height = 6*objects['b'][i],
							angle = objects['theta'][i] * 180. / np.pi,
							linewidth = 0.3)
				e.set_facecolor('none')
				e.set_edgecolor('gold')
				ax.add_artist(e)

				#add also the flux in microjansky (divide by 1e3)
				#nJy to microJy
				ax.text(objects['x'][i]+2, objects['y'][i]+2, round(objects['flux'][i]/1e3, 1), color = 'gold', fontsize = source_flux_fontsize)

		#set limits
		ax.set_xlim((0, imgsize-1))
		ax.set_ylim((imgsize-1, 0)) #reverse, because image

		return ax

	def image_filenames(self, nn, i, bigsmall_ID = None):
		if bigsmall_ID == None:
			truename = f'{nn.final_pred_loc}{i}_true_output.fits'
			inputname = f'{nn.final_pred_loc}{i}_input.fits'
			predname = f'{nn.final_pred_loc}{i}_prediction.fits'
		else:
			truename = f'{nn.final_pred_loc}{i}_{bigsmall_ID}_true_output.fits'
			inputname = f'{nn.final_pred_loc}{i}_{bigsmall_ID}_input.fits'
			predname = f'{nn.final_pred_loc}{i}_{bigsmall_ID}_prediction.fits'

		return [truename, inputname, predname]

	def savepredictions(self, nn, epoch = -1, n_files = 16):
		"""
		Save a number of predictions to a fits file. Do this in
		batches so that we do not get a memory error
		"""

		print('Saving predictions...')

		#loop over batches the size of the neural network batch size
		#most logical, as the number of prediction images has to
		#be a multiple of the batch size anyways
		for batch_counter in tqdm(range(0, n_files, nn.n_batchsize)):
			#load batch of files for predictions
			X_pred, Y_pred, WCS_array, bigsmall_ID_array = nn.training_generator.data_generation(nn.image_IDs_prediction_set[batch_counter:batch_counter+nn.n_batchsize], getvalidation_metadata = True)

			predictions = nn.predict(X_pred)

			#take inverse log if necessary
			if nn.log_images:
				predictions = 10**predictions
				Y_pred = 10**Y_pred
				if epoch == -1:
					X_pred = 10**X_pred

			#save predictions, input and true output files to fits
			for i, pred in enumerate(tqdm(predictions)):
				img_id = batch_counter + i

				names = self.image_filenames(nn, img_id, bigsmall_ID = bigsmall_ID_array[i])

				#save true output
				SU.savefits(Y_pred[i]*nn.scale_value, names[0], wcs = WCS_array[i])

				#save input data (only when at the end of training)
				if epoch == -1:
					#concatenate the split inputs
					SU.savefits(np.swapaxes(concatenate_split_arrays(X_pred, i), 0, 2)*nn.scale_value, names[1], wcs = WCS_array[i])

				#save prediction
				SU.savefits(pred*nn.scale_value, names[2], wcs = WCS_array[i])

	def plotLearningStats(self, nn, train_loss, test_loss, binsize = 128):
		"""
		Plot information on the progression of learning (train and test loss)
		"""

		#plot with two axes:
		fig, ax1 = plt.subplots()

		#bin the loss in bins of size binsize (number of batches)
		train_loss_binned = []
		for i in range(0, len(train_loss)-binsize, binsize):
			train_loss_binned.append(np.mean(train_loss[i:i+binsize]))
		train_loss_binned = np.array(train_loss_binned)

		#plot train loss
		lns1 = ax1.plot(np.linspace(0, nn.n_epochs, len(train_loss_binned)),
					train_loss_binned,
					label = f'Train loss, binsize {binsize}',
					color = 'orange', linewidth = 1)
		#plot test loss
		lns2 = ax1.plot(np.linspace(0, nn.n_epochs, len(test_loss)),
					test_loss, label = 'Test loss', color = 'blue')

		ax1.set_ylabel('Loss')
		ax1.grid(linestyle = '--')

		#always use a logarithmic y scale if using the mean squared error
		#this shows the decrease in the log after a large number of epochs
		#better. Not used for binary cross-entropy, as this can get
		#negative values
		if nn.loss_function is 'mean_squared_error':
			ax1.set_yscale('log')

		ax1.set_xlabel('Epoch')
		ax1.legend(loc = 'best')

		#set x axis labels to integers only - half epochs do not make sense
		ax1.xaxis.set_major_locator(MaxNLocator(integer=True))
		plt.title('Train and test loss')
		plt.savefig(f'{nn.prediction_loc}loss.png',
							dpi=300,
							bbox_inches='tight')
		plt.close()

	def save_loss(self, nn, run, path_out, train_loss, test_loss):
		"""
		Write loss to file
		"""
		# Save train and test loss as 2D numpy array binary file
		# 1st element = train loss, 2nd element = test loss
		train_test = np.asarray([train_loss, test_loss])
		np.save(f'{nn.prediction_loc}train_test_loss.npy',train_test)

	def save_model_settings(self, nn, settings, keys):
		"""
		Save the model settings to a csv file. The settings are given as
		a dictionary
		"""

		#save the autoencoder settings
		csv_settings = pd.read_csv(nn.modelsettings_name)

		appenddata = []
		for k in keys:
			appenddata.append(settings[k])
		appenddata = np.array(appenddata)
		appendloc = len(csv_settings) #append to end of file

		try:
			csv_settings.loc[appendloc] = appenddata
		except ValueError:
			print ('modelsettings.csv has incorrect columns')
			print ('Trying to set ', appenddata)
			raise ValueError('Please change your modelsettings.csv file to accommodate the correct column names')

		csv_settings.to_csv(nn.modelsettings_name, index = False)

	def save_final_test_loss(self, nn):
		"""
		Save final loss on the test set after training the network
		"""

		# open the modelsettings.csv file
		csv_settings = pd.read_csv(nn.modelsettings_name)

		# the row of the current version
		current_row = len(csv_settings)-1
		if csv_settings['Version'][current_row] == nn.modelversion:
			pass
		else:
			print ("Another network was likely finished before this run.")
			print ("Finding the most recent version of the run")
			print ("Please check manually if this did not overwrite anything")
			current_row = np.where(csv_settings['Version'][::-1] == nn.modelversion)[0][0]

		# These two lines give a VIEW vs COPY warning
		# csv_settings['Final detF train'][current_row] = '{0:.2f}'.format(n.history["det(F)"][-1])
		# csv_settings['Final detF test'][current_row] = '{0:.2f}'.format(n.history["det(test F)"][-1])

		# Proper practice seems to be the following syntax
		csv_settings.loc[current_row,('Final test loss')] = '{0:.2f}'.format(nn.final_test_loss)

		csv_settings.to_csv(nn.modelsettings_name, index = False)


class source_extraction(object):
	def __init__(self, nn):
		self.filter_to_name = {'A':'vimos-u', 'B':'subaru-B', 'C':'subaru-G', 'D':'subaru-V', 'E':'subaru-r', 'F':'subaru-i','G':'subaru-z','H':'vista-Y','I':'vista-J','J':'vista-H','K':'vista-Ks', 'L':'spitzer-irac1', 'M':'spitzer-irac2','N':'spitzer-irac3','O':'spitzer-irac4','P':'spitzer-mips24','Q':'herschel-pacs70', 'R':'herschel-pacs100','S':'herschel-pacs160'}
		self.nn = nn
		self.path = nn.path

		self.perfectSubtract = 10 #It just works. -Todd Howard
		self.flux_history_filename = f'{self.nn.prediction_loc}{self.nn.flux_history_filename}'

		self.edgedist = 7

	def scale_images(self):
		"""
		Scale the images back to normal counts
		"""
		filelist = SU.makeFileList(imageloc, '.fits')

		for file in filelist:
			data = []
			with fits.open(file) as hdul:
				data = hdul[0].data

			if len(data.shape) == 2:
				data = data[None,:,:]

			SU.savefits(data*self.nn.scale_value, file)

	def previewimage(self, image_name, scaletype = 'linear'):
		filename = f'{imageloc}{image_name}'

		with fits.open(filename) as hdul:
			# hdul.info()

			if scaletype == 'linear':
				image = hdul[0].data[0]/self.nn.scale_value
			elif scaletype == 'log':
				image = np.log(hdul[0].data[0]/scale_value)

			plt.imshow(image, cmap = 'gray', vmin = 0, vmax = 7)
			plt.colorbar()
			# plt.show()

	def sep_extract(self, image, sigma = 3, perfect = False):
		"""
		Extract sources using SEP: a Python source extractor implementation
		"""
		#measure background
		bkg = sep.Background(image)

		#subtract background
		if perfect:
			newdata = image - self.perfectSubtract
		else:
			newdata = image - bkg

		#extract sources
		objects = sep.extract(newdata, sigma, err = bkg.globalrms)

		# print(objects.dtype.names)

		# available fields
		if perfect:
			objects['flux'] += self.perfectSubtract*objects['tnpix']

		#remove objects too close to the edge and which have too large theta
		objects = self.remove_edge_objects(objects, image.shape[1])

		#now extract the flux by using the sum_ellipse function
		#in this way the flux is extracted in the same way as for
		#sources for which we already give the ellipses
		data_sum, sumerr, flags = sep.sum_ellipse(newdata,
					objects['x'],
					objects['y'],
					objects['a'],
					objects['b'],
					objects['theta'],
					err = bkg.globalrms)

		#replace flux with data sum, which should be the same BUT ISN'T!
		objects['flux'] = data_sum

		return objects

	def cross_match(self, true_pos, pred_pos, true_flux, pred_flux, epoch, img_id, maxdist = 4, minflux = 5e5, floor_flux = 10e4, doprint = True):
		"""
		Cross match the positions of sources in the prediction image
		to those in the true image.

		Input:
			...
			min_flux: minimum flux for a source to be included in the output,
			in nJy (units used in the fits file)

		Output:
			...
		"""
		imagesize = 100
		edgedist = 5

		if self.nn.log_images:
			floor_flux = np.log10(floor_flux)
			minflux = np.log10(minflux)

		#array that will contain the indices of the matched objects from pred
		pred_matched_idx = np.zeros(pred_pos.shape[0], dtype = int)-1

		#loop over every prediction point
		for i in range(pred_pos.shape[0]):
			distances = np.zeros(true_pos.shape[0]) + 1000
			#loop over all the true points
			for j in range(true_pos.shape[0]):
				if true_pos[j].shape[0] > 0 and pred_pos[i].shape[0]:
					distances[j] = ((true_pos[j][0]-pred_pos[i][0])**2 + (true_pos[j][1]-pred_pos[i][1])**2)**0.5

			minloc = np.argmin(distances)

			if distances[minloc] < maxdist:
				#check if the point is not at the edge
				if pred_pos[i][0] > edgedist and pred_pos[i][0] < imagesize - edgedist and pred_pos[i][1] > edgedist and pred_pos[i][1] < imagesize - edgedist:
					pred_matched_idx[i] = minloc

		#these array will contain all matched fluxes
		true_flux_matched = []
		pred_flux_matched = []

		for i in range(pred_pos.shape[0]):
			if pred_matched_idx[i] >= 0:
				#index of the true images
				j = pred_matched_idx[i]

				true_flux_matched.append(true_flux[j])
				pred_flux_matched.append(pred_flux[i])

		return true_flux_matched, pred_flux_matched

	def remove_edge_objects(self, objects, imagesize):
		"""
		Remove sources in an objects ndarray which are too close to the edge
		and have too high theta angle of the ellipse
		"""
		x_allowed = (objects['x'] > self.edgedist) * (objects['x'] < (imagesize - self.edgedist))
		y_allowed = (objects['y'] > self.edgedist) * (objects['y'] < (imagesize - self.edgedist))
		theta_allowed = np.abs(np.rad2deg(objects['theta'])) < 90

		#both the x and y pixel coordinate must not be too close to the edge
		allowed = x_allowed * y_allowed * theta_allowed

		return objects[allowed]

	def flux_deviation_end_of_training(self, true_data, pred_data, img_id = -1, doprint = False, epoch = -1):
		"""
		Determine the deviation of the flux of sources from the flux
		of matched ground truth sources

		Input:
			image_names (string array): array containing file name of
			[ground truth, prediction], in that order.
		"""
		extraction_data = []

		for i, data in enumerate([true_data, pred_data]):
			extraction_data.append(np.array(self.run_sep(data)).T)
		extraction_data = np.array(extraction_data)

		#check if the given list contains data
		if extraction_data[0].shape[0] > 0 and extraction_data[1].shape[0] > 0:
			#cross match the two tables
			true_flux_matched, pred_flux_matched = self.cross_match(extraction_data[0][:,:2], extraction_data[1][:,:2], extraction_data[0][:,2], extraction_data[1][:,2], epoch, img_id, doprint = doprint)
		else:
			true_flux_matched, pred_flux_matched = [], []

		#save every source
		with open(self.flux_history_filename, 'a') as file:
			for i in range(len(true_flux_matched)):
				if epoch == -1:
					file.write(f'End,{img_id},{i},{true_flux_matched[i]},{ pred_flux_matched[i]}\n')
				else:
					file.write(f'{epoch},{img_id},{i},{true_flux_matched[i]},{pred_flux_matched[i]}\n')

	def plot_mean_flux_dev(self):
		"""
		Plot the progression of the mean weighted flux deviation by
		loading the csv made by flux_deviation_end_of_training
		"""
		df = pd.read_csv(self.flux_history_filename)

		epochs = np.arange(self.nn.n_epochs)

		summed_flux_dev = np.zeros(len(epochs)) #summed per epoch
		n_relevant = np.zeros(len(epochs))

		#loop over all the lines in the csv
		for i in range(len(df['Epoch'])):
			if df['Epoch'][i] != 'End':
				epoch = int(df['Epoch'][i])
				summed_flux_dev[epochs == epoch] += np.abs((df['true_flux'][i]-df['pred_flux'][i])/df['true_flux'][i])

				n_relevant[epochs == epoch] += 1

		mean_weighted_flux_dev = summed_flux_dev/n_relevant
		epochs += 1 #add 1 to epochs to let them start at 1

		#plot with two axes:
		fig, ax1 = plt.subplots()
		#makes another y-axis
		ax2 = ax1.twinx()
		#plots first line
		lns1 = ax1.plot(epochs, mean_weighted_flux_dev, color = 'red', label = 'Flux deviation')
		#plots second line
		lns2 = ax2.plot(epochs, n_relevant, color = 'black', linestyle = '--', label = '# relevant sources')
		#Changes the colour of the tick labels to match the line colour
		for tl in ax1.get_yticklabels():
			tl.set_color('red')
		for tl in ax2.get_yticklabels():
			tl.set_color('black')

		lns = lns1 + lns2
		labs = [l.get_label() for l in lns]
		ax1.legend(lns, labs, loc='best')

		ax1.set_xlabel('Epochs')
		ax1.set_ylabel('Mean weighted flux deviation')
		ax2.set_ylabel('Number of relevant sources')
		plt.title('Mean weighted flux deviation per epoch')
		ax1.grid(linestyle = '--')

		plt.xticks(epochs, epochs)

		plt.savefig(f'{self.nn.prediction_loc}Flux_deviation_per_epoch.png', dpi = 300, bbox_inches = 'tight')
		plt.close()
