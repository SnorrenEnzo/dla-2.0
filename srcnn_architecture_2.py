from keras.models import Sequential, Model
from keras.layers import Input, Conv2D, Conv2DTranspose, Flatten, Dense, Dropout, Activation, Add, LeakyReLU, BatchNormalization
from keras.optimizers import Adam, RMSprop

from keras.utils.vis_utils import plot_model #requires pydot and graphviz

from srcnn_helpers import outputf

def create_model(nn):

	'''
	DESCRIPTION -----------------------------------------------------------

	Symmetric CNN with 2 skip connections

	'''

	input = Input(shape=(nn.imgsize, nn.imgsize, nn.num_filters))

	a = Conv2D(filters=64, kernel_size=7, padding='same',
					 kernel_initializer='glorot_uniform',
					 use_bias=True)(input)
	a = BatchNormalization()(a)
	a = Activation('relu')(a)

	b = Conv2D(filters=48, kernel_size=5, padding='same',
					 kernel_initializer='glorot_uniform',
					 use_bias=True)(a)
	b = BatchNormalization()(a)
	b = Activation('relu')(b)

	b = Conv2D(filters=32, kernel_size=5, padding='same',
					 kernel_initializer='glorot_uniform',
					 use_bias=True)(b)
	b = BatchNormalization()(b)
	b = Activation('relu')(b)

	x = Conv2D(filters=32, kernel_size=3, padding='same',
					 kernel_initializer='glorot_uniform',
					 use_bias=True)(b)
	x = BatchNormalization()(x)
	x = Activation('relu')(x)

	###from here start receiving the skip connections again
	x = Conv2D(filters=32, kernel_size=5, padding='same',
					 kernel_initializer='glorot_uniform',
					 use_bias=True)(x)
	x = BatchNormalization()(x)
	x = Activation('relu')(x)

	#merge layers to form a skip connection
	y = Add()([b, x])

	y = Conv2D(filters=48, kernel_size=5, padding='same',
					 kernel_initializer='glorot_uniform',
					 use_bias=True)(y)
	y = BatchNormalization()(y)
	y = Activation('relu')(y)

	y = Conv2D(filters=64, kernel_size=7, padding='same',
					 kernel_initializer='glorot_uniform',
					 use_bias=True)(y)
	y = BatchNormalization()(y)
	y = Activation('relu')(y)

	#merge layers to form a skip connection
	z = Add()([a, y])

	z = Conv2D(filters=64, kernel_size=5, padding='same',
					 kernel_initializer='glorot_uniform',
					 use_bias=True)(z)
	z = BatchNormalization()(z)
	z = Activation('relu')(z)

	#the output layer
	z = Conv2D(filters=1, kernel_size=5, padding='same',
					 kernel_initializer='glorot_uniform',
					 activation='linear',
					 use_bias=True)(z)
	z = LeakyReLU(alpha=0.3)(z)

	SRCNN_model = Model(inputs=input, outputs=z)

	if nn.optimizer == 'Adam':
		SRCNN_model.compile(optimizer = Adam(lr = nn.learning_rate), loss = nn.loss_function)
	else:
		print('No optimizer specified...')

	#reset summary file
	with open(nn.model_summary_loc + nn.model_summary_name, 'w') as f:
		pass
	#save the summary to file
	SRCNN_model.summary(print_fn = nn.savePrint)
	#make a flow chart of the model
	plot_model(SRCNN_model, to_file=f'{nn.prediction_loc}model_arch_{nn.modelversion}.png', show_shapes=True, show_layer_names=True)

	nn.model = SRCNN_model

	#obtain the parameters of the modelsettings
	modelsettings = nn.make_modelsettings_dict()

	#save the settings to file
	of = outputf()
	of.save_model_settings(nn, modelsettings, list(modelsettings.keys()))
