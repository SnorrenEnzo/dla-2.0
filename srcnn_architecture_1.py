from keras.models import Sequential
from keras.layers import Conv2D, Conv2DTranspose, Flatten, Dense, Dropout, Activation
from keras.layers.normalization import BatchNormalization
from keras.optimizers import Adam, RMSprop

from srcnn_helpers import outputf

def create_model(nn):
	
	'''		
	DESCRIPTION -----------------------------------------------------------
	
	The final architecure we used for Deep Learning in Astronomy
	
	'''		
	SRCNN = Sequential()
	
	SRCNN.add(Conv2D(filters=16, kernel_size=5, 
					padding='same', kernel_initializer='glorot_uniform', 
					activation='relu', use_bias=True, 
					input_shape=(nn.imgsize, nn.imgsize, nn.num_filters)))

	SRCNN.add(Dropout(nn.dropout))
	
	SRCNN.add(Conv2D(filters=16, kernel_size=3, padding='same',
					 kernel_initializer='glorot_uniform', activation='relu',
					 use_bias=True))

	SRCNN.add(Dropout(nn.dropout))
	
	SRCNN.add(Conv2D(filters=16, kernel_size=3, padding='same',
					 kernel_initializer='glorot_uniform', activation='relu',
					 use_bias=True))	 

	SRCNN.add(Dropout(nn.dropout))

	#the output layer
	SRCNN.add(Conv2D(filters=1, kernel_size=3, padding='same',
					 kernel_initializer='glorot_uniform', activation='linear',
					 use_bias=True))

	if nn.optimizer == 'Adam':
		SRCNN.compile(optimizer = Adam(lr = nn.learning_rate), loss = nn.loss_function)
	else:
		print('No optimizer specified...')
	
	#reset summary file
	with open(nn.model_summary_loc + nn.model_summary_name, 'w') as f:
		pass
	#save the summary to file
	SRCNN.summary(print_fn = nn.savePrint)
	
	nn.model = SRCNN

	#obtain the parameters of the modelsettings 
	modelsettings = nn.make_modelsettings_dict()

	#save the settings to file
	of = outputf()
	of.save_model_settings(nn, modelsettings, list(modelsettings.keys()))