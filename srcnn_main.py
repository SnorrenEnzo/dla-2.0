"""
==============================================================================>
Deep Learning in Astrophysics
==============================================================================>
"""

# ========================== Import Packages ================================ #

# Standard packages
import numpy as np
import pandas as pd
# from optparse import OptionParser
import timeit

# Keras (with TensorFlow backend)
from tensorflow.python.keras.callbacks import Callback, TensorBoard, ReduceLROnPlateau # For loss_history class
# import keras
# from keras.preprocessing.image import ImageDataGenerator
from sklearn.model_selection import train_test_split

import sys
import os
from tqdm import tqdm

#own packages
import srcnn_utils as SU
from srcnn_callbacks import OutputObserver, show_inputprediction, loss_history
from srcnn_helpers import outputf, source_extraction
from srcnn_dataset_generator_NEW import DataGenerator
#load your model
from srcnn_architecture_4 import create_model


# Neural network class
class SRCNN(object):

	def __init__(self, n_epochs, n_batchsize, version, filters_used, amount_samples, log_images, remove_median, load_model, prediction_set_size):

		'''
		Initialize parameters used for training and evaluation
		'''

		###################################################################
		# Parameters that can be tweaked
		###################################################################
		# Variables given by the NN (parameters)
		self.amount_samples = amount_samples
		self.n_epochs = n_epochs
		self.n_batchsize = n_batchsize
		self.dropout = 0.5
		self.learning_rate = 1e-4
		self.loss_function = 'mean_squared_error' # 'binary_crossentropy'
		self.optimizer = 'Adam'

		#size of the cutout images
		self.imgsize = 200
		#size of the original images in the old dataloader;
		#can change if the simulated data is changed
		self.original_imgsize = 250
		self.cutout_factor = self.original_imgsize//self.imgsize

		#whether to remove the median value of the images from them
		self.remove_median = remove_median

		#this is the number of files used for making and saving prediction
		#these are separated from the train and test set early on
		#must be a multiple of the batch size
		self.prediction_set_size = prediction_set_size

		assert self.prediction_set_size % self.n_batchsize == 0, 'Prediction set size not a multiple of batchsize!'

		#how much the pixel values are scaled by. Not normalization!
		#best value determined by Jonah for:
		#old dataset: 1e5
		#new dataset: 100
		self.scale_value = 1
		#whether to use log scaling of the images
		self.log_images = log_images
		if self.log_images:
			print ('Taking the log10 of the images, so setting the scale value = 1')
			self.scale_value = 1

		#size of test set as fraction of total data; only used for
		#determining the loss
		self.test_size = 0.1

		#the number of batches after which the loss is checked
		self.n_batches_loss_plot = 1

		###################################################################
		# Parameters that are automatically set/do not need to be tweaked
		###################################################################
		#parameters only used when using the data generator
		self.training_generator = None
		self.validation_generator = None

		self.verbose = 1

		#version of the neural network
		self.modelversion = version

		#which filters were used
		self.filters_used = filters_used
		self.num_filters = len(self.filters_used)

		# Variables created by the class later on
		self.model = None
		#image IDs of the prediction set; needed for SExtractor
		self.image_IDs_prediction_set = None

		self.final_test_loss = np.nan

		#number of images to plot after every epoch
		self.after_epoch_plot_images = 8

		###################################################################
		# File names and paths
		###################################################################
		#location of the data
		self.path = '/data/DLC_Team_Right_Model_data/NEW/NEW/'

		#file names of the flux deviation file and the file containing
		#all object fluxes
		self.flux_history_filename = 'flux_deviation.csv'

		#the file in which the model settings will be saved
		self.modelsettings_name = 'modelsettings.csv'
		#location where the model summaries will be saved
		self.model_summary_loc = 'Model_summaries/'
		#name of the model summary file
		self.model_summary_name = f'Model_summary_{self.modelversion}.txt'

		self.image_ids_filename = './existing_image_IDs_NEW.npy'

		#location where the models are saved
		self.modelloc = 'Models/'

		#location of the inter-epoch predictions
		self.prediction_loc = f'Predictions_{self.modelversion}/'
		#name of the directory in the prediction folder where the
		#final predictions are saved
		self.final_pred_loc = f'{self.prediction_loc}Final_predictions/'

		#check if desired folders are present
		SU.checkFolders([self.model_summary_loc, self.prediction_loc, self.final_pred_loc])

		#make a header for the flux deviation file
		with open(f'{self.prediction_loc}{self.flux_history_filename}', 'w') as f:
			f.write('Epoch,Image_ID,source_ID,true_flux,pred_flux\n')

		if load_model is not None:
			self.model = SU.saveLoadModel(f'Predictions_{load_model}/model.h5', load=True)
		else:
			create_model(self)

	def make_modelsettings_dict(self):
		"""
		Make the dictionary of the value for the modelsettings file
		"""
		modelsettings = {'Version' : self.modelversion,
					'Num filters' : self.num_filters,
					'n epochs' : self.n_epochs,
					'Batch size' : self.n_batchsize,
					'Dropout' : self.dropout,
					'Loss function' : self.loss_function,
					'Learning rate' : self.learning_rate,
					'Optimizer' : self.optimizer,
					'Amount of samples': self.amount_samples,
					'Test size': self.test_size,
					'Remove median': self.remove_median,
					'Log images': self.log_images,
					'Scale value': self.scale_value,
					'Imgsize': self.imgsize,
					'Original imgsize': self.original_imgsize,
					'Filters used': '_'.join(self.filters_used),
					'Final test loss': self.final_test_loss
					}

		return modelsettings

	def savePrint(self, s):
		"""
		Write a given Keras model summary to file
		"""

		with open(self.model_summary_loc + self.model_summary_name, 'a') as f:
			print(s, file = f)


	def train(self, X_train, Y_train, X_test, Y_test, tensorboard=False, log_dir=None):

		'''
		DESCRIPTION -----------------------------------------------------------

		Train the NN.

		ATTRIBUTES ------------------------------------------------------------

		X_train --> features of the training set
		Y_train --> values of the training set (1 for HVS and 0 for normal star)
		X_test  --> features of the test set
		Y_test  --> values of the test set (1 for HVS and 0 for normal star)

		'''

		if self.verbose > 0: print ('\nTraining neural network...' )

		#custom made callbacks
		prog_info = loss_history(self, self.model) # check loss every n batches
		showinput = show_inputprediction(1, self, X_test, Y_test, self.prediction_loc, self.imgsize, self.filters_used)

		#Keras callback
		reduce_lr = ReduceLROnPlateau(monitor = 'val_loss',
						factor = 0.2, patience = 3, verbose = 1, min_lr = 1e-8)

		if tensorboard:
			self.model.fit(X_train, Y_train, validation_data=(X_test, Y_test),
				shuffle=True, epochs=self.n_epochs, batch_size=self.n_batchsize,
				callbacks=[TensorBoard(log_dir=log_dir)], verbose=self.verbose)
		else:
			#fitting using data generator. Data is augmented on the CPU, in
			#parallel with the training on the GPU. This reduces slowdown
				self.model.fit_generator(generator = nn.training_generator,
							validation_data = nn.validation_generator,
							epochs=self.n_epochs,
							use_multiprocessing = True,
							workers = 6,
							callbacks=[prog_info,showinput,reduce_lr])

		return prog_info

	def predict(self, x, batch_size = None, verbose = 0, steps = None):

		'''
		DESCRIPTION -----------------------------------------------------------

		Make image prediction.

		'''

		prediction = self.model.predict(x, batch_size, verbose, steps)

		return prediction



# ========================= Run Neural Network ========================== #

def initialization(run, n_epochs, n_batchsize, amount_samples, filters_used, target_filter, log_images, remove_median, load_model, prediction_set_size):
	print("Run:", run)
	print("Number of epochs:", n_epochs)
	print("Batchsize:", n_batchsize)
	print('-'*40)

	##############################
	# Call neural network object #
	##############################
	nn = SRCNN(n_epochs, n_batchsize, run, filters_used, amount_samples, log_images, remove_median, load_model, prediction_set_size)

	#check if all the required folders exist
	SU.checkFolders([nn.modelloc])


	X_train, Y_train = [], []

	params = {'dim': (nn.imgsize, nn.imgsize),
			'shuffle': True,
			'filters_used': filters_used,
			'target_filter': target_filter,
			'path': nn.path,
			'nn': nn}

	# Image IDs is a list containing tuples (big_imageID,small_imageID)
	# big_imageID ranges from 1,255 and small_imageID from 0,420
	image_IDs = np.load(nn.image_ids_filename)

	#fix the way the image IDs are shuffled; useful when loading a
	#previously trained model and continuing training
	np.random.seed(1923)
	#shuffle the IDs
	np.random.shuffle(image_IDs)

	#check if the number of files to train/test on + those for predictions
	#is not larger than the total amount of data available
	if (amount_samples + nn.prediction_set_size) > len(image_IDs):
		correct_amount_samples = len(image_IDs) - nn.prediction_set_size
		raise ValueError(f'Amount of train & test samples + prediction set size larger than the amount of data available. Please decrease amount_samples to {correct_amount_samples}')

	#separate the set for making predictions
	nn.image_IDs_prediction_set = image_IDs[amount_samples:amount_samples+nn.prediction_set_size]

	#slice image IDs by the amount of data you want to use
	#-> seperates prediction set from train and test set
	image_IDs = image_IDs[:amount_samples]

	#split the train and test image IDs; the IDs are not shuffled
	train_IDs, test_IDs = train_test_split(image_IDs, test_size = nn.test_size)

	nn.training_generator = DataGenerator(train_IDs, **params) # training set
	nn.validation_generator = DataGenerator(test_IDs, **params)	# test set

	#load a small batch as prediction images which are plotted after
	#every epoch; the validation set is loaded separately in the
	#savepredictions() function in srcnn_helpers
	X_test, Y_test = nn.training_generator.data_generation(nn.image_IDs_prediction_set[:nn.n_batchsize], smaller_set = nn.after_epoch_plot_images)

	print(len(X_test))

	return nn, X_train, Y_train, X_test, Y_test

def doTrain(nn, X_train, Y_train, X_test, Y_test, run, n_epochs, n_batchsize, filters_used):
	starttime = timeit.default_timer()

	# Start training, if tensorboard=True enables web interface
	# To use: open terminal and type: tensorboard --logdir=/data1/osinga/tmpdir
	prog_info = nn.train(X_train, Y_train, X_test, Y_test, tensorboard=False,log_dir='/home/s1546449/Desktop/DeepLearningAstronomy/tflogdir')
	train_loss = prog_info.train
	test_loss = prog_info.test

	duration = timeit.default_timer() - starttime

	# End values of the loss function
	try:
		loss_train_final = train_loss[-1]
		loss_test_final = test_loss[-1]
	except AttributeError:
		loss_train_final = np.nan
		loss_test_final = np.nan

	nn.final_test_loss = loss_test_final
	# Save parameters and results to files
	t_end = 0

	of = outputf()

	of.save_final_test_loss(nn) # save final test loss in modelsettings
	#save loss progression
	of.save_loss(nn, run, nn.modelloc, train_loss, test_loss)
	#save model
	SU.saveLoadModel(f'{nn.prediction_loc}model.h5', model=nn.model, save=True)

	#save a large number of predictions for later analysis;
	#inverse log is being performed inside this function if necessary
	#files are loaded inside this function in small batches
	of.savepredictions(nn, n_files = nn.prediction_set_size)

	#save the filters used
	with open(f'{nn.prediction_loc}filters_used.txt', 'w') as f:
		f.write('_'.join(nn.filters_used))

	#plot how the learning went
	of.plotLearningStats(nn, train_loss, test_loss)

	#plot the progression of the flux deviation
	SEx = source_extraction(nn)
	#plot the mean flux deviation
	SEx.plot_mean_flux_dev()

	# Print results
	print ('\n======================== Output Parameters ======================== \n')
	print ("loss on training set: %.5f" %loss_train_final)
	print ("loss on test set: %.5f" %loss_test_final)
	print(f'Runtime: {round(duration/60, 2)} min')
	print(f'Run: {run}')
	print ("-"*40)


filters_used = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I']

target_filter = 'I'

# print ('Run number %s, log_images = '%sys.argv[1],sys.argv[2])
# run = int(sys.argv[1])

#### Main parameters that can be tweaked
run = 14 #14
n_epochs = 20 #20
#use batchsize = 16 on 6GB GPUs, batchsize = 32 on 12GB GPUs
n_batchsize = 32

#the number of images to use. 96600 in old set; 160080 in new set
amount_samples = 159000
#The number of files used for making and saving predictions, must be a
#multiple of the batchsize.
prediction_set_size = 800
######################################################################
# How these numbers work:
# Train set: 90% of amount_samples
# Test set: 10% of amount_samples
# Prediction set (end of training): prediction_set_size
#		-> NOT extracted from the amount_samples like the test set
# Predictions after end of every epoch: after_epoch_plot_images
# 		-> The first 8 images of the prediction set
######################################################################

log_images = False #best results: False
remove_median = False #best results: True; not needed for new data though
#To load a model, just fill in the run number of the desired model.
#To not load a model, use None
load_model = None

nn, X_train, Y_train, X_test, Y_test = initialization(run, n_epochs, n_batchsize, amount_samples, filters_used, target_filter, log_images, remove_median, load_model, prediction_set_size)

doTrain(nn, X_train, Y_train, X_test, Y_test, run, n_epochs, n_batchsize, filters_used)
