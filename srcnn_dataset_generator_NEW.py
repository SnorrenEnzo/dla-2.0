# Code adapted from:
# https://stanford.edu/~shervine/blog/keras-how-to-generate-data-on-the-fly

import numpy as np
# import tensorflow.python.keras as keras
from tensorflow.python.keras.utils import Sequence
from astropy.io import fits
from astropy.wcs import WCS
from astropy.time import Time

import sys
'''
This file shows the DataGenerator in action,
fully working for different input/target filters.
Batch size must be a multiple of 32.

'''

filter_to_name = {'A':'subaru-B', 'B':'subaru-V', 'C':'subaru-i', 'D':'vista-Y', 'E':'vista-Ks',
'F':'spitzer-irac2','G':'spitzer-irac4','H':'spitzer-mips24','I':'herschel-pacs100'}

# the old P is now H, the old K is now E, and the old L is now G, old Q is now I
# the rest you should check for yourself!!!!!!

class DataGenerator(Sequence):
	'''Generates data for keras '''

	def __init__(self, list_IDs, filters_used, target_filter, path, nn, dim = (200, 200), shuffle=True):

		self.dim = dim
		if nn.n_batchsize % nn.cutout_factor**2 != 0:
			error = "Batch size must be divisible by cutout_factor"
			raise ValueError(error)

		# the number of large images to load per batch
		self.local_batch_size = nn.n_batchsize//nn.cutout_factor**2

		self.list_IDs = list_IDs # image IDs
		self.shuffle = shuffle
		self.filters_used = filters_used
		self.num_filters = len(filters_used)
		self.target_filter = target_filter
		self.path = path
		self.log_images = nn.log_images
		self.cutout_factor = nn.cutout_factor

		self.nn = nn
		self.on_epoch_end()

	def __len__(self):
		'''
		Denotes number of batches per epoch. Commonly set to
		#samples/batch_size so that the model sees training examples at most once per epoch
		'''
		return int(np.floor(len(self.list_IDs) / self.local_batch_size))

	# Each call requests a batch index between 0 and the total number of batches

	# When the batch corresponding to a given index is called,
	# the generator executes the __getitem__ method to generate it.
	def __getitem__(self, index):
		'''Generate one batch of data'''

		# Generate indexes of the batch
		indexes = self.indexes[index*self.local_batch_size:(index+1)*self.local_batch_size]

		# Find list of IDs
		list_IDs_temp = [self.list_IDs[k] for k in indexes]

		# Generate data
		X, Y = self.data_generation(list_IDs_temp)

		return X, Y

	def on_epoch_end(self):
		self.indexes = np.arange(len(self.list_IDs))
		if self.shuffle == True:
			np.random.shuffle(self.indexes)

	def split_images(self, my_matrix):
		"""
		Split images; CURRENTLY NOT USED
		"""
		horizontal_split = np.hsplit(my_matrix,self.cutout_factor)
		images = []
		for i in range(len(horizontal_split)):
			# list of images
			current = np.vsplit(horizontal_split[i],self.cutout_factor)
			images.append(current)

		# reshape to (n,100,100)
		images = np.asarray(images).reshape(self.cutout_factor**2,*self.dim)
		return images

	def slice_images(self, my_matrix):
		return my_matrix[:self.dim[0],:self.dim[0]]

	def data_generation(self, list_IDs_temp, smaller_set = None, getvalidation_metadata = False):
		"""
		Generates data containing batch_size samples' # X : (n_samples, *dim, num_filters)

		If getvalidation_metadata is True, then also the WCS and original filename will be
		returned
		"""
		# Check if the list of image IDs is actually enough images to fill one batch
		assert len(list_IDs_temp) >= self.local_batch_size, f"Cannot fill one batch of size {self.local_batch_size*self.cutout_factor**2} with {len(list_IDs_temp)*self.cutout_factor**2} examples, please increase list of image IDs"

		# Initialization; these arrays will be returned
		X = np.empty((len(list_IDs_temp)*self.cutout_factor**2, *self.dim, self.num_filters)) #e.g., (64,100,100,4)
		Y = np.empty((len(list_IDs_temp)*self.cutout_factor**2, *self.dim, 1))

		if getvalidation_metadata:
			# WCS_array = np.empty(len(list_IDs_temp)*self.cutout_factor**2)
			WCS_array = []
			# bigsmall_ID_array = np.empty(len(list_IDs_temp)*self.cutout_factor**2, dtype = str)
			bigsmall_ID_array = []

 		# Generate data
		for i, (big_imageID, small_imageID) in enumerate(list_IDs_temp):
			# generate sorted filenames list
			filenames = [f'{self.path}{big_imageID}/{fil}_{small_imageID}_{filter_to_name[fil]}_sci_uniform.fits' for fil in sorted(self.filters_used)]

			for j, filename in enumerate(filenames):
				# Store samples
				# print (filename)
				with fits.open(filename) as hdulist:
					image = hdulist[0].data # (400,400) image

					#only get WCS and filename from the first file, as all others
					#will be identical
					if getvalidation_metadata and j == 0:
						header = hdulist[0].header

						#the field entries need to be converted from strings to floats
						header['CDELT1'] = float(header['CDELT1'])
						header['CDELT2'] = float(header['CDELT2'])
						header['CRPIX1'] = float(header['CRPIX1'])
						header['CRPIX2'] = float(header['CRPIX2'])
						header['CRVAL1'] = float(header['CRVAL1'])
						header['CRVAL2'] = float(header['CRVAL2'])

						#and the time to astropy time
						# print(header['DATE-OBS'], type(header['DATE-OBS']))
						header['DATE-OBS'] = f'2000-01-01T{header["DATE-OBS"]}'
						# header['DATE-OBS'] = Time('1971-01-01T00:00:00', format='isot', scale='utc')


						WCS_array.append(WCS(header))
						#the small image ID actually also contains the large
						#image ID
						bigsmall_ID_array.append(f'{big_imageID}-{small_imageID}')


					#slice into the right dimension (INEFFICIENT)
					images = self.slice_images(image)

					# if np.isnan(images.any()): print ("NAN FOUND @@@@")

					# print (filename, images.shape)

					if self.nn.remove_median:
						images -= np.median(images)

					if self.log_images:
						images = np.log10(images)

					X[i*self.cutout_factor**2:(i+1)*self.cutout_factor**2,:,:,j] = images

				# if we've arrived at the filter we're trying to predict, store the perfect image
				if filename[len(f'{self.path}{big_imageID}/')] == self.target_filter:
					# Store labels (Y)
					filename = filename.replace('sci','perfect')
					with fits.open(filename) as image:
						image = image[0].data # (400,400) image

						#slice into the right dimension (INEFFICIENT)
						images = self.slice_images(image)

						if self.nn.remove_median:
							images -= np.median(images)

						if self.log_images:
							images = np.log10(images)

						Y[i*self.cutout_factor**2:(i+1)*self.cutout_factor**2,:,:,0] = images

		#slice the array to a smaller set if desired
		if smaller_set is not None:
			X = X[:smaller_set]
			Y = Y[:smaller_set]

		if getvalidation_metadata:
			return [X[:,:,:,:3], X[:,:,:,3:5], X[:,:,:,5:7], X[:,:,:,7:]], Y, WCS_array, np.array(bigsmall_ID_array)
		else:
			return [X[:,:,:,:3], X[:,:,:,3:5], X[:,:,:,5:7], X[:,:,:,7:]], Y
