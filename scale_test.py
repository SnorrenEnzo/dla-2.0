import numpy as np 
from astropy.io import fits
from astropy.stats import sigma_clipped_stats

import matplotlib as mpl
mpl.use('agg')
import matplotlib.pyplot as plt

from tqdm import tqdm

mpl.rc('image', cmap='cubehelix')
plt.rc('text', usetex=True)

def plot_filternames(filters, filters_wl):
	xlims = plt.xlim()
	ylims = plt.ylim()

	#indicate filter names
	# plt.text(xlims[0]*1.1, ylims[0]*1.5, 'Filter:')
	for i in range(len(filters)):
		plt.text(filters_wl[i]-filters_wl[i]/20, 4e4, filters[i], fontsize = 7)

def spectrum_laddies(path, filters, filter_names, filters_wl, filters_D, n_images = 100):
	#xmin, xmax = 290, 310
	#ymin, ymax = 190, 205

	sci_filt = filters
	sci_filtnames = filter_names
	sci_wf = filters_wl

	perf_filt = filters[15:]
	perf_filtnames = filter_names[15:]
	perf_wf = filters_wl[15:]

	fig, ax = plt.subplots(2,1, sharex=True)
	
	#loop over several images
	for img_id in tqdm(range(0,n_images)):
		count = []
		max_value = []
		for i in range(len(sci_filt)):
			hdu = fits.open(f'{path}{sci_filt[i]}_1_{sci_filtnames[i]}_sci_{img_id}.fits')
			im = hdu[0].data
			flux = np.sum(im)

			hdu.close()

			count.append(flux/(400*400))
			max_value.append(np.max(im))

		total_wl = sci_wf# + perf_wf

		ax[0].scatter(total_wl, count, s = 6, edgecolors = '#411CAB', facecolors='none', linewidth = 0.5)

		ax[1].scatter(total_wl, max_value, s = 6, edgecolors = '#411CAB', facecolors='none', linewidth = 0.5)
		
	ax[0].axhline(1e5,0,1, linestyle='--', color='k')
	ax[0].set_ylabel('Counts/pixel')
	ax[0].set_xscale('log')
	ax[0].set_yscale('log')
	plot_filternames(filters, filters_wl)
	ax[0].set_title('Average number of counts per pixel, per image')

	ax[1].axhline(1e5,0,1, linestyle='--', color='k')
	ax[1].set_ylabel('Max pixel value')
	ax[1].set_xlabel('$\lambda$ ($\mu$m)')
	ax[1].set_xscale('log')
	ax[1].set_yscale('log')
	ax[1].set_title('Maximum pixel value per image filter')
	#set limits
	ylims = plt.ylim()
	plt.ylim((2e4, ylims[1]))

	plot_filternames(filters, filters_wl)



	plt.savefig('Image_pixel_statistics.png', dpi = 300, bbox_inches = 'tight')
	# plt.show()
	plt.close()

def count_distribution(path, filters, filter_names):
	
	inds = np.random.randint(0,420,size=10)
	fig, axs = plt.subplots(4,4, sharey=True, figsize=(10,10))
	ax = axs.ravel()

	for i in range(len(filters[:16])):
		ims = np.zeros((10,400,400))
		for j, ind in enumerate(inds):
			hdu = fits.open(path+filters[i]+'_1_'+filter_names[i]+'_sci_'+str(ind)+'.fits')
			ims[j,:,:] = np.copy(hdu[0].data)
			hdu.close()
		ax[i].set_title(filter_names[i])
		ax[i].hist(ims.flatten(), bins=50)
		ax[i].set_xlim(5e4,2e5)
		#ax[i].set_xscale('log')
		ax[i].set_yscale('log')

	plt.tight_layout()
	# plt.show()
	plt.close()

def scale_images():
	vis_image = 'A_1_vimos-u_sci_0.fits'
	FIR_image = 'Q_1_herschel-pacs70_sci_0.fits'

	hdu = fits.open(path+vis_image)
	im = hdu[0].data
	mean, median, std = sigma_clipped_stats(im, sigma=3.0, iters=10)
	print(std)

	fig, axs = plt.subplots(2, 3, figsize=(15,8))
	ax = axs.ravel()

	ax[0].set_title('Linear')
	ax[0].imshow(im, vmin=mean-20*std)

	ax[1].set_title('Log')
	ax[1].imshow(np.log(im), vmin=np.log(median-20*std))

	ax[2].set_title('Squared')
	ax[2].imshow(im**2, vmin=(mean-20*std)**2)

	ax[3].set_title('Arcsinh')
	ax[3].imshow(np.arcsinh(im), vmin=np.arcsinh(mean-20*std))

	ax[4].set_title('Square root')
	ax[4].imshow(np.sqrt(im), vmin=np.sqrt(mean-20*std))
	
	ax[5].hist(im.flatten(), bins=100)
	ax[5].set_xscale('log')
	ax[5].set_yscale('log')

	plt.tight_layout()
	plt.savefig('scale_test_vis.png', dpi=300)
	# plt.show()
	plt.close()

def main():
	# path = '/disks/strw18/DLC_Team_Right_Model_data/Basic_4/1/'
	path = '/data/DLC_Team_Right_Model_data/Basic_4/1/'

	filters = np.array(['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H',
			   'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P',
			   'Q','R','S'])
	filter_names = np.array(['vimos-u', 'subaru-B', 'subaru-g', 'subaru-V', 'subaru-r', 'subaru-i', 'subaru-z', 
			   'vista-Y','vista-J', 'vista-H', 'vista-Ks', 'spitzer-irac1', 'spitzer-irac2', 'spitzer-irac3', 
			   'spitzer-irac4', 'spitzer-mips24','herschel-pacs70','herschel-pacs100','herschel-pacs160'])
	filters_wl = np.array([0.37, 0.44, 0.477, 0.55, 0.628, 0.768, 0.9105, 1.02, 1.252, 
				  1.645, 2.147, 3.5, 4.49, 5.73, 7.872, 24, 70, 100, 160])
	filters_D = np.array([8.2, 8.2, 8.2, 8.2, 8.2, 8.2, 8.2, 4.1, 4.1, 4.1, 4.1, 0.85, 0.85, 0.85, 0.85, 0.85, 3.5, 3.5, 3.5])

	filters_used = np.array(['B','D','F','H','K','M','O','P','Q'])
	fil_loc = []
	for i in range(len(filters)):
		if filters[i] in filters_used:
			fil_loc.append(i)

	fil_loc = np.array(fil_loc)

	#count_distribution(path, filters, filter_names)
	spectrum_laddies(path, filters_used, filter_names[fil_loc], filters_wl[fil_loc], filters_D[fil_loc])

if __name__ == '__main__':
 	main() 