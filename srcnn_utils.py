"""
These are loosely connected functions and classes used by the SRCNN code.*
"""

import numpy as np
import os
from astropy.io import fits

from tensorflow.python.keras.models import load_model

def makeFileList(mypath, extension):
	"""
	Makes list of files with certain file extension

	Input: mypath (str), extension (str): for example .fits

	Returns: str array (location of files)
	"""
	import glob

	objectfiles = glob.glob(mypath + '*' + extension)
	objectfiles = np.sort(objectfiles)

	return objectfiles

def shuffle(a, b):
	# Shuffles arrays a and b in unison

	assert len(a) == len(b)
	p = np.random.permutation(len(a))
	return a[p], b[p]


def checkFolders(folders):
	"""
	Check whether folders are present and creates them if necessary
	"""
	for folder in folders:
		if not os.path.exists(folder):
			print(f'Making directory {folder}')
			os.makedirs(folder)

def e_scale(x, b = 10**5):
	return 1 - np.exp(-x/b)

def zscale_norm(arr, z1, z2, range = 255):
	"""
	Rescale an image within zscale range
	"""
	arr = (arr - z1) / np.abs(z2 - z1)
	arr[arr > 1] = 1
	arr = arr * 255

	return arr

def savefits(data, fitsname, wcs = None):
	"""
	Save a single fits file. If a WCS is given, then it is added to the header
	"""

	#remove fits file if already present. Otherwise it will give an
	#error
	if os.path.isfile(fitsname):
		os.remove(fitsname)

	if len(data.shape) == 3 and data.shape[2] == 1:
		writedata = data[:,:,0]
	else:
		writedata = data

	#rescale if the original data was normalized
	if wcs == None:
		fits.writeto(fitsname, writedata)
	else:
		header = wcs.to_header()

		hdu = fits.ImageHDU(data = writedata, header = header)
		hdu.writeto(fitsname)

		# fits.writeto(fitsname, writedata, header = header)

def show_image(image, save = False, epoch = None):
	from astropy.visualization import ZScaleInterval

	interval = ZScaleInterval()
	interval.get_limits(image[:,:,0])
	plt.imshow(interval(image[:,:,0]),origin='lower')
	if save:
		plt.savefig('./predictions/prediction_%i.png'%epoch)
		plt.close()
	else:
		plt.show()

def fake_nn():
	"""
	directly start a neural network to claim a GPU
	"""

	from keras.models import Sequential
	from keras.layers import Dense

	print('Starting fake NN')
	X = np.arange(10)
	X = X[:, None]
	Y = np.arange(10)

	tempNN = Sequential()
	tempNN.add(Dense(1, activation = 'relu', input_shape = (1,)))
	tempNN.compile(optimizer = 'adadelta', loss = 'mean_squared_error')

	tempNN.fit(X, Y, epochs = 1, batch_size = 1, verbose = 0)

	del tempNN, X, Y
	print('Finished with fake NN\n')


# This function is needed to generate the list of imageIDs that should be given to the DataGenerator
# So it is not part of the DataGenerator. Recommended to load this list from disk (save=True)
def generate_image_IDs(path,save=False):
	"""
	Checks if a file exists and then makes the list of image IDs
	being tuples of (big_imageID,small_imageID) where
	big_imageID is the ID of the folder and (1-255)
	small_imageID is the ID of the cutouts (0-420)

	Returns:

	list of tuple (big_imageID,small_imageID) of existing files

	"""
	image_IDs = [(k,l) for k in range(1,255) for l in range(0,420)]

	# Only use one filter since we assume if one filter exists the others do too
	filter_to_name = {'P':'spitzer-mips24'}
	filters_used = ['P']

	existing_image_IDs = []

	for i, (big_imageID, small_imageID) in enumerate(image_IDs):
		filenames = [f'{path}{big_imageID}/{fil}_{big_imageID}_{filter_to_name[fil]}_sci_{small_imageID}.fits' for fil in sorted(filters_used)]
		filename = filenames[0] # testing purposes
		if not os.path.exists(filename):
			pass # file is missing
		else:
			existing_image_IDs.append((big_imageID,small_imageID))

	if save:
		np.save('./existing_image_IDs.npy',existing_image_IDs)

	return existing_image_IDs

def saveLoadModel(filepath, model=None, save=False, load=False):
	"""
	Load or save a model easily given a path+filename. Filenames must have the format 'model.h5'.
	This saves/load model architecture, weights, loss function, optimizer, and optimizer state.

	Returns:
	model if load=True, else just saves the input model
	"""
	if save:
		print('Saving model to {}'.format(filepath))
		model.save(filepath)
	if load:
		if not os.path.exists(filepath):
			print('Cannot find specified model, check if path or filename is correct')
			return
		print('Loading model from {0}'.format(filepath))
		model = load_model(filepath)

		return model
