import os
import numpy as np
from tqdm import tqdm
from astropy.io import fits	

"""
The new data is structured as follows:

In the directory 
/disks/strw18/DLC_Team_Right_Model_data/NEW/

there are folders numbered from 0 to 1937 (big_imageID)
each folder contains 9 filters:
A,B,C,D,E,F,G,H,I (for the names see the dict below)

The filters in folder 0 are numbered from 0 to 99 (small_imageID)
The filters in folder 1 are numbered from 100 to 199 (small_imageID)
The filters in folder 2 are numbered from 200 to 299 (small_imageID)
etc..

So from 0+100*big_imageID to 99+100*big_imageID

The names are as follows

{path}{big_imageID}/{fil}_{small_imageID}_{filter_to_name[fil]}_sci_uniform.fits

e.g., /DLC_Team_Right_Model_data/NEW/0/A_0_subaru-B_sci_uniform.fits

The perfect images are found with

{path}{big_imageID}/{fil}_{small_imageID}_{filter_to_name[fil]}_perfect_uniform.fits


160236 image IDs found, thus 160236 images that have every filter.
BUT some of these images have 0 bytes. So after adding this as a check
160129 image IDs found, thus 160129 images that have every filter and are not empty files.
BUT some of these images don't have the 'perfect' image, So after adding this as a check
160114 image IDs found, thus 160114 usable images with target filter I


"""

def try_load_fits(fname):
	#do another check by trying to open the file
	try:
		a = fits.open(fname)
		if np.isnan(a[0].data).any():
			a.close()
			
			return False
		a.close()

		return True
	except (OSError, KeyError, TypeError) as e:
		tqdm.write(f'Failed to load file {fname}')

		return False

def generate_image_IDs(path,save=False):
	""" 
	Checks if a file exists and then makes the list of image IDs
	being tuples of (big_imageID,small_imageID) where 
	big_imageID is the ID of the folder and (1-255)
	small_imageID is the ID of the cutouts (0-420)

	Returns:

	list of tuple (big_imageID,small_imageID) of existing files

	"""

	#max value of big image ID
	max_big_ID = 1937#default is 1937
								# big_imageID			# small_imageID
	image_IDs = [(k,l) for k in range(0,max_big_ID) for l in range(0+100*k,100+100*k)]

	filter_to_name = {'A':'subaru-B', 
						'B':'subaru-V', 
						'C':'subaru-i', 
						'D':'vista-Y', 
						'E':'vista-Ks',
						'F':'spitzer-irac2',
						'G':'spitzer-irac4',
						'H':'spitzer-mips24',
						'I':'herschel-pacs100'}	

	target_filters = ['I']

	existing_image_IDs = []
	amount_empty_files = 0
	# empty_or_corrupt = []

	for i, (big_imageID, small_imageID) in enumerate(tqdm(image_IDs)):
		#get all the filenames for this image in different filters
		filenames = [f'{path}{big_imageID}/{fil}_{small_imageID}_{filter_to_name[fil]}_sci_uniform.fits' for fil in sorted(filter_to_name.keys())]

		#run over the filters
		add_ID = True #whether to add the IDs to the list
		for fname in filenames:
			if not os.path.exists(fname):
				# print ('Filter missing:', fname)
				#stop and do not add IDs if a single filter or all files
				#are missing
				add_ID = False
				break
			elif os.stat(fname).st_size < 1e3:
				tqdm.write(f'Filter missing because empty file: {fname}')
				#stop and do not add IDs if a single filter or all files
				#are missing
				amount_empty_files += 1
				add_ID = False
				break
			
			add_ID = try_load_fits(fname)
			if not add_ID: break
			
			# Also check if the 'perfect' image is available for filter I
			if fname[len(f'{path}{big_imageID}/')] in target_filters:
				fname = fname.replace('sci','perfect')
				if not os.path.exists(fname):
					tqdm.write(f'Filter missing perfect image: {fname}')
					#stop and do not add IDs if a single filter or all files
					#are missing
					add_ID = False
					break

				add_ID = try_load_fits(fname)
				if not add_ID: break
		
		if add_ID:
			existing_image_IDs.append((big_imageID,small_imageID))

	if save:
		np.save('./existing_image_IDs_NEW.npy',existing_image_IDs)

	print ('Amount of empty files:',amount_empty_files)
	return existing_image_IDs#, empty_or_corrupt

duranium = True

if duranium:
	path = '/data/DLC_Team_Right_Model_data/NEW/'
else:
	path = '/disks/strw18/DLC_Team_Right_Model_data/NEW/'

existing_image_IDs = generate_image_IDs(path)
print ('Number of existing image IDs found:  ', len(existing_image_IDs))
np.save('./existing_image_IDs_NEW.npy',existing_image_IDs)