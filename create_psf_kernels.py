import numpy as np
from astropy.io import fits
import matplotlib
from scipy.signal import convolve2d
import scipy
from matplotlib import pyplot as plt


"""
Apparently a zoom of 8 is what we need

"""

def test_to_show_that_it_works():
	""" Historic purposes """

	psfpath = '/disks/strw18/DLC_Team_Right_Model_data/psfs/'
	psfpath = './psfs/' # duranium

	# We assume that the -vesta is the one we need
	psf = fits.open(psfpath + 'herschel-pacs70-vesta.fits')[0].data
	assert psf.shape[0] == psf.shape[1]

	# upsample the PSF *5 	
	# psf = psf/(8.**2)

	psf_upsampled = scipy.ndimage.zoom(psf,7.7,order=3)#order=

	# Trying different interpolation schemes
	# psf_upsampled = scipy.misc.imresize(psf,800,interp='cubic')

	fig, ax = plt.subplots(3)
	ax[0].imshow(psf)
	ax[0].set_title('Original PSF')
	ax[1].imshow(psf_upsampled)
	ax[1].set_title('PSF upsampled * 8')
	# ax[2].imshow(psf_upsampled - psf_used_in_simulation)
	# plt.show()
	plt.close()
	print (np.sum(psf_upsampled))
	psf = psf_upsampled/np.sum(psf_upsampled)

	imagedir = '/disks/strw18/DLC_Team_Right_Model_data/Basic_4/1/'
	imagedir = '/data/DLC_Team_Right_Model_data/Basic_4/1/' # duranium

	science_image = 'Q_1_herschel-pacs70_sci_368.fits'
	perfect_image = 'Q_1_herschel-pacs70_perfect_368.fits'

	science_image = fits.open(imagedir+science_image)[0].data
	perfect_image = fits.open(imagedir+perfect_image)[0].data

	pad_perfect_with_zeros = np.pad(perfect_image,psf.shape[0]//2,'median')
	print (pad_perfect_with_zeros.shape) # (464, 464)
	print (psf.shape) # (31, 31)

	# For some reason flip (axis=None doesnt work on duranium)
	flip_psf = np.flip(psf,axis=0)
	flip_psf = np.flip(flip_psf,axis=1)


	# Do 'machine learning convolution'
	convolution = np.tile(np.nan,perfect_image.shape)
	for i in range(0,perfect_image.shape[0]): # position X in data
		for j in range(0,perfect_image.shape[1]): # position Y in data
			convolution[i,j] = np.sum(pad_perfect_with_zeros[i:i+psf.shape[0]
													,j:j+psf.shape[1] ] * flip_psf)
#	convolution = convolution[]

	fig, ax = plt.subplots(2,2)

	ax[0,0].imshow(science_image)
	ax[0,0].set_title('Science Image')
	ax[0,1].imshow(perfect_image)
	ax[0,1].set_title('Perfect Image')

	npconvolve = convolve2d(perfect_image,psf,mode='same', boundary='fill', fillvalue=perfect_image.min())
	ax[1,0].imshow(npconvolve)
	ax[1,0].set_title('Perfect Image convolved with psf 8x upsampled')

	ax[1,1].imshow(convolution)
	ax[1,1].set_title('Perfect Image machine learning convolved')
	plt.savefig('./testpsf.png')
	plt.show()

	plt.imshow(convolution - science_image)
	plt.colorbar()
	plt.title('Machine learning convolved minus perfect image')
	plt.savefig('./testpsf_subtract.png')
	plt.show()

	plt.imshow(npconvolve - science_image)
	plt.colorbar()
	plt.title('Numpy convolved minus perfect image')
	plt.savefig('./testpsf_subtractnp.png')
	plt.show()


test_to_show_that_it_works()

