from keras.models import Sequential, Model
from keras.layers import Input, Conv2D, Conv2DTranspose, Flatten, Dense, Dropout, Activation, Add
from keras.optimizers import Adam, RMSprop


from srcnn_helpers import outputf

def create_model(nn):
	
	'''		
	DESCRIPTION -----------------------------------------------------------
	
	Symmetric CNN with 2 skip connections
	
	'''		
	'''
	SRCNN = Sequential()
	
	SRCNN.add(Conv2D(filters=16, kernel_size=3, 
					padding='same', kernel_initializer='glorot_uniform', 
					activation='relu', use_bias=True, 
					input_shape=(nn.imgsize, nn.imgsize, nn.num_filters)))

	SRCNN.add(Dropout(nn.dropout))
	
	SRCNN.add(Conv2D(filters=16, kernel_size=3, padding='same',
					 kernel_initializer='glorot_uniform', activation='relu',
					 use_bias=True))

	SRCNN.add(Dropout(nn.dropout))
	
	SRCNN.add(Conv2D(filters=16, kernel_size=3, padding='same',
					 kernel_initializer='glorot_uniform', activation='relu',
					 use_bias=True))	 
	#the output layer
	SRCNN.add(Conv2D(filters=1, kernel_size=3, padding='same',
					 kernel_initializer='glorot_uniform', activation='linear',
					 use_bias=True))
	'''

	input = Input(shape=(nn.imgsize, nn.imgsize, nn.num_filters))
	a = Conv2D(filters=48, kernel_size=3, padding='same',
					 kernel_initializer='glorot_uniform', activation='relu',
					 use_bias=True)(input)

	a = Dropout(nn.dropout)(a)

	b = Conv2D(filters=32, kernel_size=3, padding='same',
					 kernel_initializer='glorot_uniform', activation='relu',
					 use_bias=True)(a)

	b = Dropout(nn.dropout)(b)

	b = Conv2D(filters=16, kernel_size=3, padding='same',
					 kernel_initializer='glorot_uniform', activation='relu',
					 use_bias=True)(b)

	b = Dropout(nn.dropout)(b)

	x = Conv2D(filters=16, kernel_size=3, padding='same',
					 kernel_initializer='glorot_uniform', activation='relu',
					 use_bias=True)(b)

	###from here start receiving the skip connections again

	x = Conv2D(filters=16, kernel_size=3, padding='same',
					 kernel_initializer='glorot_uniform', activation='relu',
					 use_bias=True)(x)

	x = Dropout(nn.dropout)(x)

	#merge layers to form a skip connection
	y = Add()([b, x])

	y = Conv2D(filters=32, kernel_size=3, padding='same',
					 kernel_initializer='glorot_uniform', activation='relu',
					 use_bias=True)(y)

	y = Dropout(nn.dropout)(y)

	y = Conv2D(filters=48, kernel_size=3, padding='same',
					 kernel_initializer='glorot_uniform', activation='relu',
					 use_bias=True)(y)

	y = Dropout(nn.dropout)(y)

	#merge layers to form a skip connection
	z = Add()([a, y])

	#the output layer
	z = Conv2D(filters=1, kernel_size=3, padding='same',
					 kernel_initializer='glorot_uniform', activation='relu',
					 use_bias=True)(z)

	SRCNN_model = Model(inputs=input, outputs=z)

	if nn.optimizer == 'Adam':
		SRCNN_model.compile(optimizer = Adam(lr = nn.learning_rate), loss = nn.loss_function)
	else:
		print('No optimizer specified...')
	
	#reset summary file
	with open(nn.model_summary_loc + nn.model_summary_name, 'w') as f:
		pass
	#save the summary to file
	SRCNN_model.summary(print_fn = nn.savePrint)
	
	nn.model = SRCNN_model

	#obtain the parameters of the modelsettings 
	modelsettings = nn.make_modelsettings_dict()

	#save the settings to file
	of = outputf()
	of.save_model_settings(nn, modelsettings, list(modelsettings.keys()))