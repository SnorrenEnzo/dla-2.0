# Code adapted from:
# https://stanford.edu/~shervine/blog/keras-how-to-generate-data-on-the-fly

import numpy as np
import keras
from astropy.io import fits

'''
This file shows the DataGenerator in action,
fully working for different input/target filters.
Batch size must be a multiple of 16.

'''

filter_to_name = {'A':'vimos-u', 'B':'subaru-B', 'C':'subaru-G', 'D':'subaru-V', 'E':'subaru-r',
'F':'subaru-i','G':'subaru-z','H':'vista-Y','I':'vista-J','J':'vista-H','K':'vista-Ks', 'L':'spitzer-irac1',
'M':'spitzer-irac2','N':'spitzer-irac3','O':'spitzer-irac4','P':'spitzer-mips24','Q':'herschel-pacs70',
'R':'herschel-pacs100','S':'herschel-pacs160'}

class DataGenerator(keras.utils.Sequence):
	'''Generates data for keras '''

	def __init__(self, list_IDs, filters_used, target_filter, path, nn, dim = (100,100), shuffle=True):

		self.dim = dim
		if nn.n_batchsize % nn.cutout_factor**2 != 0:
			error = "Batch size must be divisible by cutout_factor, \
					because images are 400 x 400, but we use n x n images"
			raise ValueError(error)

		# the number of large images to load per batch
		self.local_batch_size = nn.n_batchsize//nn.cutout_factor**2

		self.list_IDs = list_IDs # image IDs
		self.shuffle = shuffle
		self.filters_used = filters_used
		self.num_filters = len(filters_used)
		self.target_filter = target_filter
		self.path = path
		self.log_images = nn.log_images
		self.cutout_factor = nn.cutout_factor

		self.nn = nn
		self.on_epoch_end()

	def __len__(self):
		'''
		Denotes number of batches per epoch. Commonly set to
		#samples/batch_size so that the model sees training examples at most once per epoch
		'''
		return int(np.floor(len(self.list_IDs) / self.local_batch_size))

	# Each call requests a batch index between 0 and the total number of batches

	# When the batch corresponding to a given index is called,
	# the generator executes the __getitem__ method to generate it.
	def __getitem__(self, index):
		'''Generate one batch of data'''

		# Generate indexes of the batch
		indexes = self.indexes[index*self.local_batch_size:(index+1)*self.local_batch_size]

		# Find list of IDs
		list_IDs_temp = [self.list_IDs[k] for k in indexes]

		# Generate data
		X, Y = self.data_generation(list_IDs_temp)

		return X, Y

	def on_epoch_end(self):
		self.indexes = np.arange(len(self.list_IDs))
		if self.shuffle == True:
			np.random.shuffle(self.indexes)

	def split_images(self, my_matrix):
		horizontal_split = np.hsplit(my_matrix,self.cutout_factor)
		images = []
		for i in range(len(horizontal_split)):
			# list of images
			current = np.vsplit(horizontal_split[i],self.cutout_factor)
			images.append(current)

		# reshape to desired image dimensions
		images = np.asarray(images).reshape(self.cutout_factor**2,*self.dim)
		return images

	def data_generation(self, list_IDs_temp):
		'''Generates data containing batch_size samples' # X : (n_samples, *dim, num_filters)'''
		# Check if the list of image IDs is actually enough images to fill one batch
		assert len(list_IDs_temp) >= self.local_batch_size, f"Cannot fill one batch of size {self.local_batch_size*self.cutout_factor**2} with {len(list_IDs_temp)*self.cutout_factor**2} examples, please increase list of image IDs"

		# Initialization
		X = np.empty((len(list_IDs_temp)*self.cutout_factor**2, *self.dim, self.num_filters)) #e.g., (64,100,100,4)
		Y = np.empty((len(list_IDs_temp)*self.cutout_factor**2, *self.dim, 1))

 		# Generate data
		for i, (big_imageID, small_imageID) in enumerate(list_IDs_temp):
			# generate sorted filenames list
			filenames = [f'{self.path}{big_imageID}/{fil}_{big_imageID}_{filter_to_name[fil]}_sci_{small_imageID}.fits' for fil in sorted(self.filters_used)]

			for j, filename in enumerate(filenames):
				# Store samples
				with fits.open(filename) as image:
					image = image[0].data # (400,400) image

					#split into subimages
					images = self.split_images(image)

					if self.nn.remove_median:
						images -= np.median(images)

					#remove negative and zero values
					images[images < 1] = 1

					if self.log_images:
						images = np.log10(images)

					X[i*self.cutout_factor**2:(i+1)*self.cutout_factor**2,:,:,j] = images

				# if we've arrived at the filter we're trying to predict, store the perfect image
				if filename[len(f'{self.path}{big_imageID}/')] == self.target_filter:
					# Store labels (Y)
					filename = filename.replace('sci','perfect')
					with fits.open(filename) as image:
						image = image[0].data # (400,400) image

						#split into subimages
						images = self.split_images(image)

						if self.nn.remove_median:
							images -= np.median(images)

						#remove negative and zero values
						images[images < 1] = 1

						if self.log_images:
							images = np.log10(images)

						Y[i*self.cutout_factor**2:(i+1)*self.cutout_factor**2,:,:,0] = images

		if self.nn.scale_value is not None:
			return X/self.nn.scale_value, Y/self.nn.scale_value

		return X, Y

'''
#HOW TO USE
from sklearn.model_selection import train_test_split
from keras.models import Sequential
from keras.layers import Conv2D, Conv2DTranspose, Flatten, Dense, Dropout, Activation
from keras.optimizers import Adam, RMSprop

path = '/disks/strw18/DLC_Team_Right_Model_data/Basic_4/'

filters_used = ['P','K','L']
target_filter = 'P'

params = {'dim': (100,100),
			'batch_size': 64,
			'shuffle': True,
			'filters_used': filters_used,
			'target_filter': target_filter}
# Image IDs is a list containing tuples (big_imageID,small_imageID)
# big_imageID ranges from 1,255 and small_imageID from 0,420
image_IDs = np.load('./existing_image_IDs.npy')

# Split training and test set
test_size = 0.2
train_IDs, test_IDs = train_test_split(image_IDs,test_size=0.2)

partition = {'train': train_IDs,
			'validation': test_IDs}
# Generators
training_generator = DataGenerator(partition['train'], **params)
validation_generator = DataGenerator(partition['validation'], **params)

# ==============================================================
# Temporary parameters and network
# to show that this setup works
num_filters = len(filters_used)
n_conv_kernel = 6
ker_size = 5
learning_rate = 0.01

SRCNN = Sequential()

SRCNN.add(Conv2D(filters=n_conv_kernel, kernel_size=ker_size, padding='same',
				 kernel_initializer='glorot_uniform', activation='relu',
				 use_bias=True, input_shape=(100, 100, num_filters)))

SRCNN.add(Conv2D(filters=n_conv_kernel, kernel_size=5,padding='same'
	,kernel_initializer='glorot_uniform',activation='relu'
	,use_bias=True))

SRCNN.add(Conv2D(filters=1, kernel_size=5,padding='same'
	,kernel_initializer='glorot_uniform',activation='relu'
	,use_bias=True))

SRCNN.compile(optimizer = Adam(lr = learning_rate), loss = 'mean_squared_error')

print (SRCNN.summary())

# Train model on dataset
hist = SRCNN.fit_generator(generator=training_generator,
                    validation_data=validation_generator,
                    use_multiprocessing=True,
                    workers=6, epochs = 10)
'''
