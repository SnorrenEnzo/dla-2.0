import os
import numpy as np
from tqdm import tqdm

def generate_image_IDs(path,save=False):
	""" 
	Checks if a file exists and then makes the list of image IDs
	being tuples of (big_imageID,small_imageID) where 
	big_imageID is the ID of the folder and (1-255)
	small_imageID is the ID of the cutouts (0-420)

	Returns:

	list of tuple (big_imageID,small_imageID) of existing files

	"""
	image_IDs = [(k,l) for k in range(1,255) for l in range(0,420)]

	filter_to_name = {'A':'vimos-u', 'B':'subaru-B', 'D':'subaru-V', 'E':'subaru-r',
	'F':'subaru-i','G':'subaru-z','H':'vista-Y','I':'vista-J','J':'vista-H','K':'vista-Ks', 'L':'spitzer-irac1',
	'M':'spitzer-irac2','N':'spitzer-irac3','O':'spitzer-irac4','P':'spitzer-mips24','Q':'herschel-pacs70',
	'R':'herschel-pacs100','S':'herschel-pacs160'}	
	existing_image_IDs = []

	for i, (big_imageID, small_imageID) in enumerate(tqdm(image_IDs)):
		#get all the filenames for this image in different filters
		filenames = [f'{path}{big_imageID}/{fil}_{big_imageID}_{filter_to_name[fil]}_sci_{small_imageID}.fits' for fil in sorted(filter_to_name.keys())]

		#run over the filters
		add_ID = True #whether to add the IDs to the list
		for fname in filenames:
			if not os.path.exists(fname):
				print ('Filter missing:', fname)
				#stop and do not add IDs if a single filter or all files
				#are missing
				add_ID = False
				break 
		
		if add_ID:
			existing_image_IDs.append((big_imageID,small_imageID))

	if save:
		np.save('./existing_image_IDs.npy',existing_image_IDs)

	return existing_image_IDs

duranium = True

if duranium:
	path = '/data/DLC_Team_Right_Model_data/Basic_4/'
else:
	path = '/disks/strw18/DLC_Team_Right_Model_data/Basic_4/'

existing_image_IDs = generate_image_IDs(path)
print ('Number of existing image IDs found:  ', len(existing_image_IDs))
np.save('./existing_image_IDs.npy',existing_image_IDs)