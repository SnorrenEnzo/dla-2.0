import numpy as np
import pandas as pd
import matplotlib
matplotlib.use('agg')
import matplotlib.pyplot as plt

from tqdm import tqdm

import argparse
import os

from tensorflow.python.keras.models import load_model

def checkFolders(folders):
	"""
	Check whether folders are present and creates them if necessary
	"""
	for folder in folders:
		if not os.path.exists(folder):
			print(f'Making directory {folder}')
			os.makedirs(folder)

def loadModel(filepath):
	"""
	Load a model easily given a path+filename. Filenames must have the format 'model.h5'.
	This loads model architecture, weights, loss function, optimizer, and optimizer state.

	Returns:
	model
	"""
	if not os.path.exists(filepath):
		print('Cannot find specified model, check if path or filename is correct')
		return
	print('Loading model from {0}'.format(filepath))
	model = load_model(filepath)
	print('Loaded model')

	return model

def plot_kernel_collection(kernel_collection, j, filter, kernel_saveloc, version):
	"""
	Plot kernels of a single filter layer (3D dimension of the kernels)
	"""

	kernelmin = np.min(kernel_collection)
	kernelmax = np.max(kernel_collection)

	#normalize if we are handling the RGB image
	rgb = (len(kernel_collection.shape) == 4 and kernel_collection.shape[2] == 3)

	if rgb:
		#normalize the kernel collection
		kernel_collection = (kernel_collection - kernelmin)/(kernelmax - kernelmin)

	#determine the number of kernels
	if rgb:
		n_kernels = kernel_collection.shape[3]
	else:
		n_kernels = kernel_collection.shape[2]

	#set the number of kernels horizontally and vertically
	n_hor = 8
	n_ver = n_kernels/n_hor

	fig = plt.figure(figsize = (n_hor * 2, n_ver * 2))

	#plot all the kernels
	for i in tqdm(range(n_kernels), desc = 'Adding kernel subplots'):
		ax = fig.add_subplot(n_ver, n_hor, i+1)

		if rgb:
			#we do not want a gray cmap for the RGB image
			plt.imshow(kernel_collection[:,:,:,i])
		else:
			plt.imshow(kernel_collection[:,:,i], cmap = 'gray', vmin = kernelmin, vmax = kernelmax)
		plt.axis('off')

	if rgb:
		title = f'i, V, B (RGB) kernels'
	else:
		title = f'Filter {j} ({filter}) kernels'
	#add a main title for the subfigures
	fig.suptitle(title, fontsize = 35)

	plt.subplots_adjust(wspace = 0.1, hspace = 0.1)
	if rgb:
		plt.savefig(f'{kernel_saveloc}{version}_layer1_kernels_iVB_RGB.png', bbox_inches = 'tight')
	else:
		plt.savefig(f'{kernel_saveloc}{version}_layer1_kernels_{j}_{filter}.png', bbox_inches = 'tight')
	plt.close()


def main(version):
	#layer_id = None means that we are looking at the split inputs
	layer_id = None


	project_dir = '/home/s1530194/DLA_Team_Right/'
	if layer_id is None:
		kernel_saveloc = f'./Kernels/version{version}_splitinput/'
	else:
		kernel_saveloc = f'./Kernels/version{version}_layer_{layer_id}/'
	checkFolders([kernel_saveloc])


	model_path = f'{project_dir}Predictions_{version}/model.h5'
	model = loadModel(model_path)

	#load the filters
	filters = pd.read_csv(f'{project_dir}simulation_filters_NEW.csv')


	if layer_id is not None:
		#the case that we are dealing with a single input network

		#extract the kernels; shape: (7, 7, 9, 32) for the first layer
		kernels = np.array(model.layers[layer_id].get_weights())[0]

		#plot for every filter
		for j in tqdm(range(9)):
			#plot the kernels for the B, V and i filters as an RGB image.

			if j > 2:
				plot_kernel_collection(kernels[:,:,j], j, filters["Instrument filter"][j], kernel_saveloc, version)
			elif j == 2:
				#take the first three filters
				rgb_kernels = kernels[:,:,:3]
				#the ordering is BVi, but for RGB we want iVB. So we reverse the filters
				rgb_kernels = rgb_kernels[:,:,::-1]

				plot_kernel_collection(rgb_kernels, -1, filters["Instrument filter"][j], kernel_saveloc, version)

				#last axis should be length 3
				# rgb_kernels = np.swapaxes(rgb_kernels, 0, 2)
			else:
				tqdm.write(f'Skipping {j} due to being a i or V filter for the RGB image')
	else:
		#we are working with a split input network
		#the parameters below can be changed to extract the correct layers

		#IDs of the first conv2D layers of the various input branches
		BVi_id = 10
		NIR_id = 11
		MIR_id = 4
		FIR_id = 5

		layer_ids = [BVi_id, NIR_id, MIR_id, FIR_id]
		branchnames = ['BVi', 'NIR', 'MIR', 'FIR']

		filter_counter = 0

		#extract kernels
		for i in tqdm(range(len(layer_ids)), desc = '4 inputs'):
			kernels = np.array(model.layers[layer_ids[i]].get_weights())[0]

			if kernels.shape[2] == 3:
				#the BVi track, plot as RGB

				#the ordering is BVi, but for RGB we want iVB. So we reverse the filters
				rgb_kernels = kernels[:,:,::-1]

				fil = f'{filters["Instrument filter"][i]}-{branchnames[i]}'
				plot_kernel_collection(rgb_kernels, -1, fil, kernel_saveloc, version)

				filter_counter = 3
			else:
				#plot each filter separately
				fil = f'{filters["Instrument filter"][filter_counter]}-{branchnames[i]}'
				plot_kernel_collection(kernels[:,:,0], filter_counter, fil, kernel_saveloc, version)
				filter_counter += 1

				fil = f'{filters["Instrument filter"][filter_counter]}-{branchnames[i]}'
				plot_kernel_collection(kernels[:,:,1], filter_counter, fil, kernel_saveloc, version)
				filter_counter += 1

if __name__ == '__main__':
	parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
	parser.add_argument('--version', help = 'the run to analyse.', default = 12)
	args = parser.parse_args()

	main(int(args.version))
