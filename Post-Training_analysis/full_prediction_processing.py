import numpy as np
import os
import sep #for source extraction
from tqdm import tqdm
import pandas as pd

from astropy.io import fits
from astropy.wcs import WCS

from scipy.stats import norm, cauchy, gamma

import matplotlib
matplotlib.use('agg')
import seaborn as sns #for the KDEplot
import matplotlib.pyplot as plt
import matplotlib.colors as colors
import matplotlib.cm as cmx
from matplotlib.patches import Ellipse
import matplotlib.ticker as mticker

#for managing fonts
from matplotlib import rcParams
import matplotlib.font_manager as fm

import sys
import argparse
import glob

# prop = fm.FontProperties(fname='../lmroman10-regular.otf')

font_dirs = ['/home/s1530194/DLA_Team_Right/', ]
font_files = fm.findSystemFonts(fontpaths = font_dirs)
font_list = fm.createFontList(font_files)
fm.fontManager.ttflist.extend(font_list)
rcParams['font.family'] = 'Latin Modern Roman'


labelfontsize = 15
titlefontsize = 17

class source_extraction(object):
	def __init__(self):
		self.perfectSubtract = 10 #It just works. -Todd Howard

		#minimum distance to the edge of a source
		self.edgedist = 7

		#the minimum predicted flux needed for inclusion in the TP etc stats
		self.total_stats_minflux = 0.3 #mJy

	def savefits(self, data, fitsname, Ymax = None):
		"""
		Save a single fits file
		"""

		#remove fits file if already present. Otherwise it will give an
		#error
		if os.path.isfile(fitsname):
			os.remove(fitsname)

		if len(data.shape) == 3 and data.shape[2] == 1:
			writedata = data[:,:,0]
		else:
			writedata = data

		#rescale if the original data was normalized
		if Ymax is not None:
			fits.writeto(fitsname, writedata * Ymax)
		else:
			fits.writeto(fitsname, writedata)

	def loadfits(self, filename, load_wcs = False):
		"""
		Load a fits file with multiple filters
		"""
		data = []
		wcs = None

		with fits.open(filename) as hdul:
			i = 0
			cont = True

			#try to go to the next image in the fits file and load it
			while cont:
				try:
					data.append(hdul[i].data)
					wcs = WCS(hdul[i].header)
				except IndexError:
					cont = False

				i += 1

		data = np.array(data)

		#load only the first filter if there are more than three. In this way
		#if we want to load input filters, we only get 1 filter or an RGB
		#combination
		if len(data.shape) > 3:
			data = data[0]

		if len(data.shape) == 1:
			try:
				data = data[1]
			except IndexError:
				pass

		if load_wcs:
			return data, wcs
		else:
			return data


	def remove_edge_objects(self, objects, imagesize):
		"""
		Remove sources in an objects ndarray which are too close to the edge
		and have too high theta angle of the ellipse
		"""
		x_allowed = (objects['x'] > self.edgedist) * (objects['x'] < (imagesize - self.edgedist))
		y_allowed = (objects['y'] > self.edgedist) * (objects['y'] < (imagesize - self.edgedist))
		theta_allowed = np.abs(np.rad2deg(objects['theta'])) < 90

		#both the x and y pixel coordinate must not be too close to the edge
		allowed = x_allowed * y_allowed * theta_allowed

		return objects[allowed]

	def cross_match(self, ref_pos, tomatch_pos, ref_flux, tomatch_flux, tomatch_flux_err, imagesize, maxdist = 3):
		"""
		Cross match the positions of sources in the prediction image
		to those in the true image.

		Input:
			...
			floor_flux (float): minimum flux of an object for it to be
			processed at all.

		Output:
			mean_deviation (float): the mean deviation of source fluxes
			in the prediction images from the ground truth.
			n_relevant (float): number of sources found with relevant fluxes
			(flux in true and prediction images larger than minflux)
			non_relevant_counter (float): number of sources with non
			relevant fluxes
			TP, FP, FN as dictionary.
		"""

		if len(ref_pos) == 0:
			return np.array([]), np.array([]), np.array([])

		#make arrays of source IDs (IDs specific for this image)
		IDs_pred = np.arange(tomatch_pos.shape[0])
		IDs_true = np.arange(ref_pos.shape[0])

		#array that will contain the indices of the matched objects from true
		#so the numbers in this array will be true source IDs
		tomatch_matched_idx = np.zeros(tomatch_pos.shape[0], dtype = int)-1

		#loop over every prediction point
		for i in IDs_pred:
			distances = np.empty(ref_pos.shape[0])
			#loop over all the true points
			for j in IDs_true:
				#obtain distances
				distances[j] = ((ref_pos[j][0]-tomatch_pos[i][0])**2 + (ref_pos[j][1]-tomatch_pos[i][1])**2)**0.5

			#find the minimum of these distances
			minloc = np.argmin(distances)
			if distances[minloc] < maxdist:
				#check if the point is not at the edge
				if tomatch_pos[i][0] > self.edgedist and tomatch_pos[i][0] < imagesize - self.edgedist and tomatch_pos[i][1] > self.edgedist and tomatch_pos[i][1] < imagesize - self.edgedist:
					#set the matched source
					tomatch_matched_idx[i] = minloc

		#these arrays will contain all matched fluxes
		ref_flux_matched = []
		tomatch_flux_matched = []
		tomatch_flux_err_matched = [] #also error on the prediction flux
		#counter for the number of objects in the prediction which are not
		#relevant in the ground truth
		non_relevant_counter = 0

		for i in IDs_pred:
			if tomatch_matched_idx[i] >= 0:
				#index of the true images
				j = tomatch_matched_idx[i]

				#find the deviation in percent
				dev = 100*(tomatch_flux[i] - ref_flux[j])/ref_flux[j]

				ref_flux_matched.append(ref_flux[j])
				tomatch_flux_matched.append(tomatch_flux[i])
				tomatch_flux_err_matched.append(tomatch_flux_err[i])

		#make a selection mask for the TP etc stats based on the minimum flux
		#parameter, which is in mJy (and the flux itself is in nJy)
		tomatch_total_stats_selection = (tomatch_flux >= self.total_stats_minflux*1e6)
		ref_total_stats_selection = (ref_flux >= self.total_stats_minflux*1e6)


		#find the number of matched sources (TP), unmatched sources in the pred
		#(FP) and unmatched sources in the true data (FN)
		#filter using the minimum flux mask
		TP = np.sum(tomatch_matched_idx[tomatch_total_stats_selection] > -1)

		#find FP by checking number of unmatched pred array elements
		FP = np.sum(tomatch_matched_idx[tomatch_total_stats_selection] < 0)

		#find FN by checking number of true IDs which are not matched
		FN = np.sum(np.isin(IDs_true[ref_total_stats_selection], tomatch_matched_idx[tomatch_total_stats_selection]) == False)
		# tqdm.write(f'TP: {TP:2}   FP: {FP:2}   FN: {FN:2}')

		return np.array(ref_flux_matched), np.array(tomatch_flux_matched), np.array(tomatch_flux_err_matched), tomatch_matched_idx, {'TP': TP, 'FP': FP, 'FN': FN}

	def sep_extract(self, image, sigma = 3, perfect = False):
		"""
		Extract sources using SEP: a Python source extractor implementation
		"""
		#measure background
		bkg = sep.Background(image)

		#subtract background
		if perfect:
			newdata = image - self.perfectSubtract
		else:
			newdata = image - bkg

		#extract sources
		objects = sep.extract(newdata, sigma, err = bkg.globalrms)

		#the available keys
		#('thresh', 'npix', 'tnpix', 'xmin', 'xmax', 'ymin', 'ymax', 'x', 'y', 'x2', 'y2', 'xy', 'errx2', 'erry2', 'errxy', 'a', 'b', 'theta', 'cxx', 'cyy', 'cxy', 'cflux', 'flux', 'cpeak', 'peak', 'xcpeak', 'ycpeak', 'xpeak', 'ypeak', 'flag')


		# print(objects.dtype.names)

		# available fields
		if perfect:
			objects['flux'] += self.perfectSubtract*objects['tnpix']

		#remove objects too close to the edge and which have too large theta
		objects = self.remove_edge_objects(objects, image.shape[1])

		#now extract the flux by using the sum_ellipse function
		#in this way the flux is extracted in the same way as for
		#sources for which we already give the ellipses
		data_sum, sumerr, flags = sep.sum_ellipse(newdata,
					objects['x'],
					objects['y'],
					objects['a'],
					objects['b'],
					objects['theta'],
					err = bkg.globalrms)


		#replace flux with data sum, which should be the same BUT ISN'T!
		objects['flux'] = data_sum
		#make new field for the flux error
		objects = np.lib.recfunctions.append_fields(objects, 'flux_err', sumerr)

		return objects

	def sep_extract_ellipses(self, image, ref_objects):
		"""
		Extract fluxes of sources with SEP (Python SExtractor)using given
		reference ellipses
		"""
		#measure background
		bkg = sep.Background(image)

		#subtract background
		newdata = image - bkg

		#extract fluxes based on ellipses
		data_sum, sumerr, flags = sep.sum_ellipse(newdata,
					ref_objects['x'],
					ref_objects['y'],
					ref_objects['a'],
					ref_objects['b'],
					ref_objects['theta'],
					err = bkg.globalrms)

		return data_sum, sumerr


	def extract_and_match(self, data, ref_objects, imagesize, use_ellipses = True):
		"""
		Extract sources with SExtractor and match them with the ground truth
		"""
		if not use_ellipses:
			#### cross match with the reference to obtain fluxes
			objects = self.sep_extract(data)

			ref_flux_matched, tomatch_flux_matched, tomatch_flux_matched_error, tomatch_matched_idx, stats = self.cross_match(np.array([ref_objects['x'], ref_objects['y']]).T,
				np.array([objects['x'], objects['y']]).T, ref_objects['flux'],
				objects['flux'], objects['flux_err'], imagesize)

			return tomatch_flux_matched, tomatch_flux_matched_error, tomatch_matched_idx, stats, objects
		else:
			#### extract flux using ellipses from the reference (ground truth
			#objects)

			#first fix some (now trivial) parameters
			ref_flux_matched = ref_objects['flux']
			tomatch_matched_idx = np.arange(len(ref_flux_matched))
			stats = {'TP': -1, 'FP': -1, 'FN': -1}

			#now extract fluxes
			tomatch_flux_matched, tomatch_flux_matched_error = self.sep_extract_ellipses(data, ref_objects)

			return tomatch_flux_matched, tomatch_flux_matched_error, tomatch_matched_idx, stats, ref_objects

	def swap_and_inverse_log(self, data, do_inverse_log = False):
		#change byte order for sep
		data = data.byteswap().newbyteorder()

		if do_inverse_log:
			return 10**data
		else:
			return data

	def checkFolders(self, folders):
		"""
		Check whether folders are present and creates them if necessary
		"""
		for folder in folders:
			if not os.path.exists(folder):
				print(f'Making directory {folder}')
				os.makedirs(folder)

class SED_analysis(object):
	def __init__(self):
		#location where the plots will be saved
		plot_savepath = './SED_plots/'

		self.SEx = source_extraction()

	def plot_SED_single_image(self, img_number, filters_used, target_filter, wavelengths_used, img_path, plot_savepath, scatter_size = 15, xlims = (0.2, 100), ylims = (1e-2, 5e3)):
		"""
		Plot the SED of a single image. This is done by loading the input,
		ground truth and prediction fit files, extracting and matching sources.
		"""
		#### determine first location of the reference objects in the ground truth
		fname = f'{img_path}{img_number}_true_output.fits'
		data = self.SEx.loadfits(fname)[0]
		data = self.SEx.swap_and_inverse_log(data)

		x_ref, y_ref, ref_flux = self.SEx.sep_extract(data)
		ref_pos = np.array([x_ref, y_ref]).T
		#make a simple reference source ID array
		ref_source_id = np.arange(0, len(x_ref))

		#### now load the positions of the input data
		fname = f'{img_path}{img_number}_input.fits'
		data = self.SEx.loadfits(fname)
		data = data.transpose(0, 2, 1)
		data = self.SEx.swap_and_inverse_log(data)

		#get fluxes
		input_fluxes, input_matched_idx = [], []
		for i in tqdm(range(data.shape[0])):
			tomatch_flux_matched, tomatch_flux_matched_error, matched_idx = self.SEx.extract_and_match(data[i], ref_pos, ref_flux)

			input_fluxes.append(tomatch_flux_matched)
			input_matched_idx.append(matched_idx)


		#### finally, load the prediction
		fname = f'{img_path}{img_number}_prediction.fits'
		data = self.SEx.loadfits(fname)[0]
		data = self.SEx.swap_and_inverse_log(data)
		pred_fluxes, pred_flux_error, pred_matched_idx = self.SEx.extract_and_match(data, ref_pos, ref_flux)


		#wavelength of the target and ground truth filter
		target_wavelength = np.array(filterdata['Central wavelength [micron]'])[filterdata['Filter letter'] == target_filter]

		#for plotting the input data, select here all the filters but the target one
		input_filter_loc = (filters_used != target_filter)

		#marker of the input data points
		inputmarker = 'o'
		inputmarkers = np.array(['o', 's', 'v'])
		#set the colourmap for the points of the different sources
		jet = plt.get_cmap('jet')
		cNorm  = colors.Normalize(vmin = 0, vmax = len(ref_source_id) - 1)
		scalarMap = cmx.ScalarMappable(norm = cNorm, cmap = jet)
		#loop over all the sources in the reference (ground truth) image and
		#plot them
		for id in ref_source_id:
			#flux of single object per wavelength
			single_object_flux = []
			#add matched fluxes to array
			for m, f in zip(input_matched_idx, input_fluxes):
				sof = np.array(f)[m == id]

				if len(sof) > 0:
					single_object_flux.append(sof[0])
				else:
					single_object_flux.append(np.nan)

			#set color of points
			colour = scalarMap.to_rgba(id)

			single_object_flux = np.array(single_object_flux)

			#only plot if there is at least 1 data point of the input images to plot
			if np.sum(np.isnan(single_object_flux)) < len(single_object_flux):
				#plot the ground truth source fluxes
				plt.scatter(target_wavelength, ref_flux[id], color = colour, s =scatter_size, marker = inputmarker)

				#plot the matched input file sources
				plt.scatter(wavelengths_used[input_filter_loc], single_object_flux[input_filter_loc], label = id, color = colour, s = scatter_size, marker = inputmarker)

				#plot the prediction source flux (only if there is a matched object)
				single_pred_object_flux = pred_fluxes[pred_matched_idx == id]

				if len(single_pred_object_flux) == 1 and np.sum(np.isnan(single_pred_object_flux)) == 0:
					plt.scatter(target_wavelength, single_pred_object_flux, color = colour, marker = 'x', s = 3*scatter_size)

			#change marker so that sources with very similar plot colours
			#can be easily distinguished
			inputmarker = inputmarkers[np.where(inputmarkers == inputmarker)[0]-2][0]

		#indicate the filter names
		plt.text(xlims[0]*1.1, ylims[0]*1.5, 'Filter:')
		for i in range(len(filters_used)):
			plt.text(wavelengths_used[i]-wavelengths_used[i]/15, ylims[0]*1.5, filters_used[i], fontsize = 10)

		plt.xscale('log')
		plt.yscale('log')
		plt.xlim(xlims)
		plt.ylim(ylims)

		plt.xlabel(r'Wavelength [$\mu m$]')
		plt.ylabel('Flux [counts]')
		plt.title(f'SED of sources found in image {img_number}\no = input, x = prediction')
		plt.legend(loc = 'upper left', ncol = 2, title = 'Source ID', prop={'size':8})

		plt.savefig(f'{plot_savepath}{img_number}_source_SED.png', dpi = 300, bbox_inches = 'tight')
		# plt.show()
		plt.close()

	def SED_incorrect_flux_correlation(self, img_number, filters_used, target_filter, wavelengths_used, img_path, plot_savepath, plot_filter = 0, scatter_size = 15, xlims = (0.2, 100), ylims = (1e-2, 5e3)):
		"""
		Plot per source in the given image for a certain filter the flux in
		that filter versus the predicted flux error.
		"""
		#### determine first location of the reference objects in the ground truth
		fname = f'{img_path}{img_number}_true_output.fits'
		data = self.SEx.loadfits(fname)[0]
		data = self.SEx.swap_and_inverse_log(data)

		x_ref, y_ref, ref_flux = self.SEx.sep_extract(data)
		ref_pos = np.array([x_ref, y_ref]).T
		#make a simple reference source ID array
		ref_source_id = np.arange(0, len(x_ref))

		#### now load the positions of the input data
		fname = f'{img_path}{img_number}_input.fits'
		data = self.SEx.loadfits(fname)
		data = data.transpose(0, 2, 1)
		data = self.SEx.swap_and_inverse_log(data)

		#get fluxes
		input_fluxes, input_matched_idx = [], []
		for i in range(data.shape[0]):
			tomatch_flux_matched, tomatch_flux_matched_error, matched_idx = self.SEx.extract_and_match(data[i], ref_pos, ref_flux)

			input_fluxes.append(tomatch_flux_matched)
			input_matched_idx.append(matched_idx)


		#### finally, load the prediction
		fname = f'{img_path}{img_number}_prediction.fits'
		data = self.SEx.loadfits(fname)[0]
		data = self.SEx.swap_and_inverse_log(data)
		pred_fluxes, pred_flux_error, pred_matched_idx = self.SEx.extract_and_match(data, ref_pos, ref_flux)


		#wavelength of the target and ground truth filter
		target_wavelength = np.array(filterdata['Central wavelength [micron]'])[filterdata['Filter letter'] == target_filter]

		#for plotting the input data, select here all the filters but the target one
		input_filter_loc = (filters_used != target_filter)

		#create dictionaries which will hold the data
		filter_flux = {}
		flux_error = {}
		for filt in filters_used[:-1]:
			filter_flux[filt] = []
			flux_error[filt] = []

		#find the flux in each filter and flux difference for every desired
		#object
		for id in ref_source_id:
			#flux of single object at the particular filter
			# single_object_flux = input_fluxes[plot_filter][input_matched_idx[plot_filter] == id]
			single_object_flux = []
			#add matched fluxes to array
			for m, f in zip(input_matched_idx, input_fluxes):
				sof = np.array(f)[m == id]

				if len(sof) > 0:
					single_object_flux.append(sof[0])
				else:
					single_object_flux.append(np.nan)

			#the prediction source flux (only if there is a matched object)
			single_pred_object_flux = pred_fluxes[pred_matched_idx == id]

			#determine the flux error (relative)
			flux_err = (ref_flux[id] - single_pred_object_flux)/ref_flux[id]

			#add the flux of the filters versus and error in the
			#predicted flux to the dicts of data to be plotted
			for i in range(len(filters_used)-1):
				if len(flux_err) > 0 and not np.isnan(single_object_flux[i]):
					filter_flux[filters_used[i]].append(single_object_flux[i])
					flux_error[filters_used[i]].append(flux_err[0])

		return filter_flux, flux_error

	def flux_correlation_all_filters(self):
		#location of the stored images
		img_path = './23_final_predictions/'
		#location where the plots will be saved
		plot_savepath = './SED_plots/'
		self.SEx.checkFolders([plot_savepath])

		#load data on the filters
		filterdata  = pd.read_csv('../simulation_filters.csv')
		lines = []
		with open('filters_used.txt', 'r') as f:
			for l in f:
				lines.append(l)
		filters_used = np.array(lines[0].replace('\n', '').split('_'))
		#find the wavelengths used
		wavelengths_used = []
		for f in filters_used:
			wavelengths_used.append(np.array(filterdata['Central wavelength [micron]'])[filterdata['Filter letter'] == f])
		wavelengths_used = np.array(wavelengths_used)

		target_filter = 'Q'

		#loop over all the images
		'''
		for img_number in tqdm(range(20)):
			self.SEx.plot_SED_single_image(img_number, filters_used, target_filter, wavelengths_used, img_path, plot_savepath)
		'''

		#these two dicts contain the data which we want to plot
		filter_flux = {}
		flux_error = {}
		for filt in filters_used:
			filter_flux[filt] = []
			flux_error[filt] = []

		for img_number in tqdm(range(2000)):
			filter_flux_singleimg, flux_error_singleimg = self.SEx.SED_incorrect_flux_correlation(img_number, filters_used, target_filter, wavelengths_used, img_path, plot_savepath)

			#extract fluxes per filter
			for filt in filters_used[:-1]:
				filter_flux[filt].extend(filter_flux_singleimg[filt])
				flux_error[filt].extend(flux_error_singleimg[filt])

		for fil in filters_used:
			print(f'{fil}: {np.corrcoef(np.array([filter_flux[fil], flux_error[fil]]))[0,1]}')

		#loop over all the filters
		for filter_id in tqdm(range(len(wavelengths_used)-1)):

			#plot all the data points
			plt.scatter(filter_flux[filters_used[filter_id]], flux_error[filters_used[filter_id]], s = 4, color = 'black', alpha = 0.3, edgecolor = 'none')

			plt.xscale('log')
			plt.yscale('symlog')
			plt.xlim((4e-2, 2e4))
			plt.ylim((-3e2, 3e2))

			plt.xlabel(f'Flux of filter {filters_used[filter_id]}')
			plt.ylabel('Flux error (true min pred / true)')
			plt.title(f'Correlation of flux error with filter {filters_used[filter_id]} flux ({round(wavelengths_used[filter_id][0], 3)} micron)')

			plt.savefig(f'{plot_savepath}filter_{filters_used[filter_id]}_rel_error_symlog_correlation.png', dpi = 300, bbox_inches = 'tight')
			plt.close()

class flux_deviation_analysis(object):
	def __init__(self, true_flux, pred_flux, true_ellipse_params, pred_ellipse_params, run, use_ellipses, selection_minflux):
		#convert fluxes to mJy (input is nJy)
		self.true_flux = true_flux/1e6
		self.pred_flux = pred_flux/1e6

		#check for flux values smaller than 1 nJy: these we do not want
		# true_allowed = self.true_flux > 1e-6
		# pred_allowed = self.pred_flux > 1e-6
		# flux_allowed = true_allowed * pred_allowed
		# self.true_flux = self.true_flux[flux_allowed]
		# self.pred_flux = self.pred_flux[flux_allowed]

		self.error = (self.pred_flux - self.true_flux)/self.true_flux

		self.true_ellipse_params = true_ellipse_params
		self.pred_ellipse_params = pred_ellipse_params


		self.run = run
		if use_ellipses:
			self.extra_fname = '_useEll'
		else:
			self.extra_fname = ''

		#offer possibility of selecting only sources with relevant (high
		#enough) flux. Units are mJy
		self.selection_minflux = selection_minflux
		self.selection = (self.pred_flux > self.selection_minflux) * (self.true_flux > self.selection_minflux)

		#determine range of values for plotting
		#by taking the mean of the 3% and 97% percentiles
		#only use those sources with an error lower than 1, otherwise the
		#lowest flux objects fall way outside the error versus true plot
		self.min_log, self.max_log = np.log10(self.percentile_limits(self.true_flux[self.error <= 1]))

		self.SEx = source_extraction()

		self.pred_true_figloc = 'Pred_true_KDE/'
		self.pdf_cdf_figloc = 'PDF_CDF/'
		self.error_vs_true_figloc = 'Error_vs_true/'
		#location where TP, FP, FN (and maybe other stats) are saved
		self.statloc = 'Statistics/'

		self.SEx.checkFolders([self.pdf_cdf_figloc,
							self.pred_true_figloc,
							self.error_vs_true_figloc,
							self.statloc])

		print(f'{len(self.true_flux)} sources of which {np.sum(self.selection)} selected with the minflux = {self.selection_minflux} mJy')

	def percentile_limits(self, data, percentile = 1):
		"""
		Set limits based on a given percentile (default = 1, so 2% and 98%)
		"""
		minvalue = np.mean(np.percentile(data, percentile))
		maxvalue = np.mean(np.percentile(data, 100 - percentile)) #100 - percentile

		return minvalue, maxvalue

	def rotmat(self, angle):
		"""
		Make a rotation matrix of size 2 with angle in degrees
		"""
		theta = np.deg2rad(angle)
		R = np.array([[np.cos(theta), -np.sin(theta)], [np.sin(theta), np.cos(theta)]])

		return R

	def plot_true_pred(self, fixlims = True):
		"""
		Plot true versus predicted flux with KDE contours
		"""
		lims = (self.min_log, self.max_log)

		x = np.log10(self.true_flux)
		y = np.log10(self.pred_flux)

		fig = plt.figure(figsize = (7, 5))
		ax = plt.subplot(111)

		#get extents of figure and add a colorbar
		l = ax.figure.subplotpars.left
		r = ax.figure.subplotpars.right
		t = ax.figure.subplotpars.top
		b = ax.figure.subplotpars.bottom
		#width and height
		w = r - l
		h = t - b

		#the actual left position where we will position the main plot
		left = 0.8

		#make colorbar axes
		#[left, bottom, width, height]
		cax = plt.axes([left + 0.02, b, 0.03, h])

			##### plot kernel density estimator result
		sns.kdeplot(x, y, shade = True, shade_lowest = False, cmap = 'Blues', ax = ax, cbar = True, cbar_ax = cax)
		#add label to colorbar
		fig.subplots_adjust(right = right)
		cax.set_ylabel('Density [sources per unit area]', fontsize = labelfontsize)

			##### plot the individual points
		# ax.scatter(x, y, s = 1, color = 'black', alpha = 0.05)

			##### plot diagonal line indicating the ideal values
		line = np.array([lims[0],np.log10(np.max(self.true_flux))])
		ax.plot(line, line, color = 'red', linewidth = 1, label = 'Ideal values')

			##### determine the line of the average true distribution
		#first rotate the data
		rot_coords = np.array([x, y]).T @ self.rotmat(45)
		x_rot = rot_coords[:,0]
		y_rot = rot_coords[:,1]

		#determine average in bins
		n_bins = 50
		avg_rot = np.zeros(n_bins)

		#determine the number of objects per bin and the bin locations
		hist_counts, bin_locs = np.histogram(x_rot, bins = n_bins)
		for i in range(n_bins):
			avg_rot[i] = np.mean(y_rot[(x_rot > bin_locs[i])*(x_rot < bin_locs[i+1])])

		#make the coordinates for the line of averages in the rotated frame
		dev_y_rot = avg_rot
		dev_x_rot = np.zeros(len(dev_y_rot))
		for i in range(len(dev_y_rot)):
			dev_x_rot[i] = (bin_locs[i] + bin_locs[i+1])/2

		#rotate back
		coords = np.array([dev_x_rot, dev_y_rot]).T @ self.rotmat(-45)
		dev_x = coords[:,0]
		dev_y = coords[:,1]

		ax.plot(dev_x, dev_y, color = 'black', linewidth = 1, label = 'Average w.r.t. diagonal')

		#set the ticks at integer numbers
		# mintick = int(self.min_log)
		# maxtick = int(self.max_log)
		ticks = np.arange(-4, 2, 1)
		ax.set_xticks(ticks)
		ax.set_yticks(ticks)

		if fixlims:
			ax.set_xlim(lims)
			ax.set_ylim(lims)

		ax.set_xlabel('log10 $F_{true}$ [mJy]', fontsize = labelfontsize)
		ax.set_ylabel('log10 $F_{pred}$ [mJy]', fontsize = labelfontsize)

		ax.grid(linestyle = ':')
		ax.set_axisbelow(True)

		ax.legend(loc = 'best')

		ax.set_title(f'True versus predicted flux with KDE', fontsize = titlefontsize)
		plt.savefig(f'{self.pred_true_figloc}{self.run}_flux_true_pred_kde{self.extra_fname}.png', dpi = 300, bbox_inches = 'tight')
		# plt.show()
		plt.close()

	def plot_true_error(self):
		"""
		Plot the error (Fpred-Ftrue)/Ftrue versus the true flux, Ftrue of every
		source. Includes 25%, 50% and 75% percentiles for bins
		"""
		plt.scatter(self.true_flux, self.error, s = 3, color = 'black', alpha = 0.15, edgecolor = 'none')

		#determine bins in which we calculate the percentiles
		log_bin_edges = np.linspace(self.min_log, self.max_log, 10)

		percentile_data = {25: [], 50: [], 75: []}
		#normal scale bin centers
		bin_centers = []
		for i in range(len(log_bin_edges)-1):
			bin_min = 10**log_bin_edges[i]
			bin_max = 10**log_bin_edges[i+1]

			bin_centers.append(np.mean([bin_min, bin_max]))

			#select the points in the bin
			loc = (self.true_flux > bin_min)*(self.true_flux < bin_max)
			error_selected = self.error[loc]

			#add the different percentiles to the percentile dict
			for key in percentile_data.keys():
				percentile_data[key].append(np.percentile(error_selected, key))

		#now plot percentiles
		percentile_colour = '#373BCC'
		plt.plot(bin_centers, percentile_data[75], linestyle = '--', color = percentile_colour, label = '75%')
		plt.plot(bin_centers, percentile_data[50], linestyle = '-', color = percentile_colour, label = '50%')
		plt.plot(bin_centers, percentile_data[25], linestyle = ':', color = percentile_colour, label = '25%')


		plt.xscale('log')
		plt.ylim((-1, 1))
		plt.xlim((10**log_bin_edges[0], 10**log_bin_edges[-1]))

		plt.grid(linestyle = ':')
		#move grid behind plot
		ax = plt.gca()
		ax.set_axisbelow(True)

		plt.legend(loc = 'best')

		plt.xlabel(r'$F_{true}$ [mJy]', fontsize = labelfontsize)
		plt.ylabel(r'$(F_{pred} - F_{true})/F_{true}$', fontsize = labelfontsize)
		plt.title('Flux error versus true flux', fontsize = titlefontsize)
		plt.savefig(f'{self.error_vs_true_figloc}{self.run}_error_vs_true{self.extra_fname}.png', dpi = 300, bbox_inches = 'tight')
		# plt.show()
		plt.close()

	def plot_pdf_cdf(self):
		"""
		Calculate the culumaltive distribution function of the predicted and
		true flux. This is done by first doing a cumulative sum and then
		multiplying by the spacing between the points
		"""

		n_bins = 100

		#### First plot a histogram (a PDF)
		hist_true = np.histogram(np.log10(self.true_flux), bins = n_bins)
		hist_pred = np.histogram(np.log10(self.pred_flux), bins = n_bins)

		#calculate bin centers
		bin_center_true = np.diff(hist_true[1])/2 + hist_true[1][:-1]
		bin_center_pred = np.diff(hist_pred[1])/2 + hist_pred[1][:-1]

		plt.step(10**bin_center_true, hist_true[0], color = 'darkblue', label = 'True flux', linewidth = 1)
		plt.step(10**bin_center_pred, hist_pred[0], color = 'crimson', label = 'Pred flux', linewidth = 1)

		plt.xscale('log')
		plt.legend(loc = 'best')
		plt.xlabel('Flux [mJy]', fontsize = labelfontsize)
		plt.ylabel('Counts', fontsize = labelfontsize)
		plt.title('Flux histogram (PDF)', fontsize = titlefontsize)
		plt.grid(linestyle = ':')

		plt.savefig(f'{self.pdf_cdf_figloc}{self.run}_PDF{self.extra_fname}.png', dpi = 300, bbox_inches = 'tight')
		plt.close()


		#### Now plot a CDF of the fluxes
		#for this, we first need to sort the fluxes
		sorted_true = np.sort(self.true_flux)
		sorted_pred = np.sort(self.pred_flux)

		#make the cumulative sum
		cumul_true = np.cumsum(sorted_true)
		cumul_pred = np.cumsum(sorted_pred)

		#we need a range of values for the x axis
		xrange = np.arange(1, len(cumul_true) + 1)

		plt.plot(xrange, cumul_true, color = 'darkblue', label = 'True flux', linewidth = 1)
		plt.plot(xrange, cumul_pred, color = 'crimson', label = 'Pred flux', linewidth = 1)

		plt.yscale('log')
		plt.legend(loc = 'best')
		plt.xlabel('Number of sources')
		plt.ylabel('Cumulative flux [mJy]')
		plt.title('Flux CDF')
		plt.grid(linestyle = ':')

		plt.savefig(f'{self.pdf_cdf_figloc}{self.run}_CDF{self.extra_fname}.png', dpi = 300, bbox_inches = 'tight')
		plt.close()

	def statistics(self, run, total_stats):
		#apply selection
		error = self.error[self.selection]

		print(f'\nRun {run}')
		print(f'Total number of sources: {len(self.true_flux)}')
		print(f'{len(error)} sources selected for statistics')
		print(f'Mean absolute error: {round(np.mean(np.abs(error)), 3)}')

		print(f'Median absolute error: {round(np.median(np.abs(error)), 3)}')

		#write TP etc stats to file or read them
		stats_fname = f'{self.statloc}{run}_stats.txt'

		if total_stats is not None:
			with open(stats_fname, 'w') as f:
				f.write(f'Statistics minimum flux threshold: {self.selection_minflux} mJy\n')
				for k in total_stats.keys():
					print(f'{k}: {total_stats[k]}')
					f.write(f'{k}: {total_stats[k]}\n')
		else:
			print('Loading old TP, FP, FN statistics!')
			with open(stats_fname, 'r') as f:
				for line in f:
					print(line, end = '')

class imagePlotter(object):
	def __init__(self):
		self.SEx = source_extraction()

		#fix min and max of img plotting in microJy
		#the data is converted from nanoJy to microJy in
		#getAllDataForImagePlots()
		self.vmin = -0.2
		self.vmax = 2

	def loadFITses(self, img_path, img_number, typename, returnfilename = False, load_wcs = False):
		"""
		Load data and swap axes.

		typename: string, options:
			'true_output'
			'prediction'
			'input'
		"""

		#because of the wildcard, loop over the folders. The wildcard is in
		#place for the case that the big and small image ID are included in the
		#filename
		path = f'{img_path}{img_number}_*{typename}.fits'

		#load the first file that fits this wildcard, as there should not be
		#any other files matching it
		filename = glob.glob(path)[0]

		#we do not want to swap axes and select the first filter if we want to
		#load the input (these are 9 filters)
		if typename is 'input':
			if load_wcs:
				data, wcs = self.SEx.loadfits(filename, load_wcs = True)
			else:
				data = self.SEx.loadfits(filename)
		else:
			if load_wcs:
				data, wcs = self.SEx.loadfits(filename, load_wcs = True)
			else:
				data = self.SEx.loadfits(filename)
			data = self.SEx.swap_and_inverse_log(data, do_inverse_log = False)

		if data.shape[0] == 1:
			data = data[0]

		if returnfilename:
			if load_wcs:
				return data, filename, wcs
			else:
				return data, filename
		else:
			if load_wcs:
				return data, wcs
			else:
				return data

	def plot_source_indicators(self, ax, objects, imgsize = 100, plotflux = True):
		"""
		Plot the source indicators of SExtractor for the per epoch
		image summary
		"""

		#sizes of text
		# source_indicator_size = 20
		source_flux_fontsize = 6

		# plot an ellipse for each object
		for i in range(len(objects)):
			#only plot if the flux is high enough
			if objects['flux'][i]/1e3 > 10:
				e = Ellipse(xy = (objects['x'][i], objects['y'][i]),
							width = 6*objects['a'][i],
							height = 6*objects['b'][i],
							angle = objects['theta'][i] * 180. / np.pi,
							linewidth = 0.7)
				e.set_facecolor('none')
				e.set_edgecolor('gold')
				ax.add_artist(e)

				#add also the flux in microjansky (divide by 1e3)
				#correct for the size of the source
				if plotflux:
					xoffset = 3 * objects['a'][i] * np.cos(objects['theta'][i]) + 1
					yoffset = 3 * objects['a'][i] * np.sin(objects['theta'][i])
					ax.text(objects['x'][i] + xoffset, objects['y'][i] + yoffset, round(objects['flux'][i]/1e3, 1), color = 'gold', fontsize = source_flux_fontsize)

		#set limits
		ax.set_xlim((0, imgsize-1))
		ax.set_ylim((imgsize-1, 0)) #reverse, because image

		return ax

	def addGrid(self, ax, imgsize):
		"""
		Add a grid to the images
		"""
		ticks = np.linspace(0, imgsize, 4, dtype = int)[1:]
		ax.set_xticks(ticks)
		ax.set_yticks(ticks)
		ax.grid(linestyle = '--', color = 'white', linewidth = 0.7)

	def remove_all_ticks(self, ax):
		"""
		Remove all tick of an axis
		"""
		#this removes the ticks, but also any associated grid
		ax.tick_params(left = False, right = False,
						bottom = False, top = False,
						labelbottom = False, labelleft = False)

	def getAllDataForImagePlots(self, img_path, img_number):
		"""
		Get all the data needed for full plotting of images:
			prediction
			ground truth
			input (RGB)
		"""
		##### Load data
		truedata = self.loadFITses(img_path, img_number, 'true_output')
		preddata = self.loadFITses(img_path, img_number, 'prediction')

		#load also the B, V and i filters (A, B and C) of the input data
		inputdata = self.loadFITses(img_path, img_number, 'input')[:3][::-1]
		#normalize data for imshow plotting
		datastd = np.std(inputdata)
		datamean = np.mean(inputdata)
		inputmin = datamean - datastd * 0.5
		inputmax = datamean + datastd * 5
		inputdata = (inputdata - inputmin)/(inputmax - inputmin)
		#clip the data between 0 and 1 as values outside this range are not
		#allowed by imshow
		inputdata = np.clip(inputdata, 0, 1)
		#now swap axes so the last axis is of length 3, the RGB one
		inputdata = np.swapaxes(inputdata, 0, 2)

		#find the imagesize
		imgsize = preddata.shape[1]

		#run SExtractor for source flux deviations
		objects_true = self.SEx.sep_extract(truedata, perfect = True)
		objects_pred = self.SEx.sep_extract(preddata, perfect = False)

		#filter sources on minimum flux and distance to the image edge
		objects_true = self.SEx.remove_edge_objects(objects_true, imgsize)
		objects_pred = self.SEx.remove_edge_objects(objects_pred, imgsize)

		#convert the true and prediction data from nanojansky to microjansky
		#inputdata is already scaled from 0 to 1 for an RGB plot
		return truedata/1e3, preddata/1e3, inputdata, objects_true, objects_pred, imgsize

	def getAxisCoordinates(self, ax):
		"""
		Get the left and bottom coordinates, and width and height of a
		matplotlib axes object, in figure coordinates (between 0 and 1)
		"""
		ax_str = str(ax)
		ax_str = ax_str.replace('AxesSubplot(', '').replace(')', '')

		l, temp = ax_str.split(',')
		b, temp = temp.split(';')
		width, height = temp.split('x')

		return float(l), float(b), float(width), float(height)

	def makeColorbar(self, ax, im, cbar_label, cbar_fontsize, mintick, maxtick, n_ticks = 3):
		"""
		Make a colorbar with the same vertical height as the plot it
		corresponds with. Also set the ticks
		"""
		l, b, w, h = self.getAxisCoordinates(ax)

		#small hack for fixing the spacing of the second column's colorbars
		correction  = 0.005
		if mintick < 0:
			correction = 0.02

		cax = plt.axes([l + w - correction, b, 0.02, h])
		#add colorbar with label
		cbar = plt.colorbar(im, cax = cax, ticks = np.linspace(mintick, maxtick, n_ticks))
		cax.set_ylabel(cbar_label, fontsize = cbar_fontsize)

	def plot_predictions(self, run, n_images = 20):
		"""
		Plot a certain number of ground truth, prediction and RGB images
		"""
		tfontsize = 10
		# fluxfilename = f'./{run}_true_pred_flux.txt'
		#location of the stored images
		img_path = f'../Predictions_{run}/Final_predictions/'
		#location where the plots will be saved
		plot_savepath = './Image_plots/'
		self.SEx.checkFolders([plot_savepath])

		for img_number in tqdm(range(n_images), desc = 'Plotting images'):
			truedata, preddata, inputdata, objects_true, objects_pred, imgsize = self.getAllDataForImagePlots(img_path, img_number)

			fig = plt.figure(figsize = (7, 6))

			#### ground truth
			ax = fig.add_subplot(221)
			ax.set_title(f'Ground truth, img {img_number}', fontsize = tfontsize)
			im = ax.imshow(truedata, cmap = 'gray', vmin = self.vmin, vmax = self.vmax)
			self.remove_all_ticks(ax)
			#plot the source indicators with flux labels
			ax = self.plot_source_indicators(ax, objects_true, imgsize = imgsize)
			#add colorbar
			self.makeColorbar(ax, im, r'Flux in $\mathrm{\mu}$Jy', labelfontsize / 1.7, 0, self.vmax)

			#### prediction
			ax = fig.add_subplot(223)
			ax.set_title(f'Prediction, img {img_number}', fontsize = tfontsize)
			im = ax.imshow(preddata, cmap = 'gray', vmin = self.vmin, vmax = self.vmax)
			self.remove_all_ticks(ax)
			#plot the source indicators with flux labels
			ax = self.plot_source_indicators(ax, objects_pred, imgsize = imgsize)
			#add colorbar
			self.makeColorbar(ax, im, r'Flux in $\mathrm{\mu}$Jy', labelfontsize / 1.7, 0, self.vmax)

			#### residuals
			ax = fig.add_subplot(222)
			ax.set_title(f'Residuals (' + r'$F_{pred} - F_{true}$' + f'), img {img_number}', fontsize = tfontsize)
			im = ax.imshow(preddata - truedata, cmap = 'RdBu', vmin = -2, vmax = 2)

			self.remove_all_ticks(ax)
			#plot the source indicators without flux labels
			ax = self.plot_source_indicators(ax, objects_pred, imgsize = imgsize, plotflux = False)
			#add colorbar
			self.makeColorbar(ax, im, r'Flux residuals in $\mathrm{\mu}$Jy', labelfontsize / 1.7, -2, 2)

			#### RGB image using A, B and C filters
			ax = fig.add_subplot(224)
			ax.set_title(f'Input i, V, B (RGB), img {img_number}', fontsize = tfontsize)
			ax.imshow(inputdata)
			self.remove_all_ticks(ax)

			plt.subplots_adjust(right = 0.9, wspace = 0.1, hspace = 0.2)
			plt.savefig(f'{plot_savepath}{run}_img_{img_number}.png', dpi = 300, bbox_inches = 'tight')
			plt.close()
			# plt.show()

	def plot_selected_imgs(self, run):
		"""
		Plot a selection of image IDs
		"""
		#the indices of the selected images
		idx_selection = [0, 4, 8, 12]

		n_images = len(idx_selection)

		fontsize = 20
		title_yloc = 1.05

		#location of the stored images
		img_path = f'../Predictions_{run}/Final_predictions/'
		#location where the plots will be saved
		plot_savepath = './Image_plots/'
		self.SEx.checkFolders([plot_savepath])

		#size of a single image in the grid, in inches
		img_size_inches = 4

		fig = plt.figure(figsize = (img_size_inches * 3, img_size_inches * n_images))

		#loop over the selected images
		for i in tqdm(range(n_images), desc = 'Adding images to figure'):
			#load the data
			truedata, preddata, inputdata, objects_true, objects_pred, imgsize = self.getAllDataForImagePlots(img_path, idx_selection[i])


			#ground truth
			ax = fig.add_subplot(n_images, 3, i*3+1)
			if i == 0:
				ax.set_title(f'Ground truth', fontsize = fontsize, y = title_yloc)
			ax.imshow(truedata, cmap = 'gray', vmin = self.vmin, vmax = self.vmax)
			self.addGrid(ax, truedata.shape[0])
			self.remove_all_ticks(ax)
			#plot the source indicators
			ax = self.plot_source_indicators(ax, objects_true, imgsize)

			#prediction
			ax = fig.add_subplot(n_images, 3, i*3+2)
			if i == 0:
				ax.set_title('Prediction', fontsize = fontsize, y = title_yloc)
			ax.imshow(preddata, cmap = 'gray', vmin = self.vmin, vmax = self.vmax)
			self.addGrid(ax, truedata.shape[0])
			self.remove_all_ticks(ax)
			#plot the source indicators
			ax = self.plot_source_indicators(ax, objects_pred, imgsize)

			#RGB image using A, B and C filters
			ax = fig.add_subplot(n_images, 3, i*3+3)
			if i == 0:
				ax.set_title('Input i, V, B (RGB)', fontsize = fontsize, y = title_yloc)
			ax.imshow(inputdata)
			self.addGrid(ax, truedata.shape[0])
			self.remove_all_ticks(ax)

		plt.subplots_adjust(hspace = 0.1, wspace = 0.005)
		plt.savefig(f'{plot_savepath}{run}_selected_images.png', dpi = 300, bbox_inches = 'tight')
		plt.close()

	def analyse_n_images(self, run, n_images, use_ellipses):
		"""
		Input:
			run: which run you are on; used for filenames
			n_images: the number of x by x images to load. This is not the
			number of samples used for the prediction set in srcnn_main, but
			the number of cutout images saved.
		"""
		def makeFileList(mypath, extension):
			"""
			Makes list of files with certain file extension

			Input: mypath (str), extension (str): for example .fits

			Returns: str array (location of files)
			"""
			import glob

			objectfiles = glob.glob(mypath + '*' + extension)
			objectfiles = np.sort(objectfiles)

			return objectfiles

		#location of the stored images
		# img_path = f'./{run}_final_predictions/'
		img_path = f'../Predictions_{run}/Final_predictions/'

		#location where the previously extracted fluxes are saved
		prev_flux_loc = 'Extracted_fluxes/'
		self.SEx.checkFolders([prev_flux_loc])

		if use_ellipses:
			fluxfilename = f'{prev_flux_loc}{run}_true_pred_flux_useEll.txt'
		else:
			fluxfilename = f'{prev_flux_loc}{run}_true_pred_flux.txt'
		axesfilename = f'{prev_flux_loc}{run}_ellipse_axes.npy'

		try:
			#### Try to load the previously extracted raw fluxes and ellipses
			data = np.loadtxt(fluxfilename)
			true_flux = data[:,0]
			pred_flux = data[:,1]

			true_ellipse_params, pred_ellipse_params = np.load(axesfilename)

			print('Loaded previously extracted data')

			total_stats = None
		except OSError:
			#### If this is not successful, then there is no data on the
			#### previously extracted sources available and the extraction needs
			#### to be done again
			true_flux = []
			pred_flux = []

			total_stats = {'TP': 0, 'FP': 0, 'FN': 0}

			if n_images == None:
				n_images = len(makeFileList(img_path, '.fits'))//3

			#gather the ellipse axes
			true_ellipse_params = {'a': [], 'b': []}
			pred_ellipse_params = {'a': [], 'b': []}

			#loop over the images and extract fluxes
			for img_number in tqdm(range(n_images), desc = 'Extracting fluxes'):
				#### load ground truth and prediction; get WCS of ground truth
				truedata, wcs = self.loadFITses(img_path, img_number, 'true_output', load_wcs = True)
				preddata, pred_fname = self.loadFITses(img_path, img_number, 'prediction', returnfilename = True)

				#when wanting to use ellipses from the prediction image,
				#we first have to extract the prediction image
				pred_objects = self.SEx.sep_extract(preddata)

				#if using ellipses, remove sources too close to the edge
				if use_ellipses:
					pred_objects = self.SEx.remove_edge_objects(pred_objects, preddata.shape[1])

				#make a simple reference source ID array
				pred_source_id = np.arange(0, len(pred_objects['flux']))

				#now extract fluxes from ground truth
				true_fluxes, true_flux_error, true_matched_idx, stats, true_objects = self.SEx.extract_and_match(truedata, pred_objects, truedata.shape[1], use_ellipses = use_ellipses)


				#match IDs; this is done in reverse (so true is matched to pred)
				#due to reverse ellipse extraction
				pred_f = []
				true_f = []
				pred_p = {'a': [], 'b': [], 'x': [], 'y': [], 'flux': [], 'flux_err': []}
				true_p = {'a': [], 'b': []}
				for id in pred_source_id:
					if id in true_matched_idx:
						#sometimes two predictions are matched to a single true
						#source; those are filtered out using [:1]
						true_loc = np.where(true_matched_idx == id)
						pred_loc = np.where(pred_source_id == id)

						true_f.extend(true_objects['flux'][true_loc][:1])
						pred_f.extend(pred_objects['flux'][pred_loc][:1])

						true_p['a'].extend(true_objects['a'][true_loc][:1])
						true_p['b'].extend(true_objects['b'][true_loc][:1])

						pred_p['a'].extend(pred_objects['a'][pred_loc][:1])
						pred_p['b'].extend(pred_objects['b'][pred_loc][:1])
						pred_p['x'].extend(pred_objects['x'][pred_loc][:1])
						pred_p['y'].extend(pred_objects['y'][pred_loc][:1])
						pred_p['flux'].extend(pred_objects['flux'][pred_loc][:1])
						pred_p['flux_err'].extend(pred_objects['flux_err'][pred_loc][:1])

				true_flux.extend(true_f)
				pred_flux.extend(pred_f)

				true_ellipse_params['a'].extend(true_p['a'])
				true_ellipse_params['b'].extend(true_p['b'])
				pred_ellipse_params['a'].extend(pred_p['a'])
				pred_ellipse_params['b'].extend(pred_p['b'])

				#also add the TP, FP, FN statistics
				for k in total_stats.keys(): total_stats[k] += stats[k]

				#### write the extracted flux with coordinates to file
				#but first get the big and small image ID, and put them in
				#the filename
				bigsmall_id_fname = pred_fname.replace('.fits', '')

				#convert coordinates to world coordinates
				ra, dec = wcs.all_pix2world(pred_p['x'], pred_p['y'], 0)

				df = pd.DataFrame(data = np.array([pred_p['x'],
									pred_p['y'],
									ra,
									dec,
									pred_p['flux'],
									pred_p['flux_err']]).T,
									columns = ['x [pixels]',
									'y [pixels]',
									'ra [deg]',
									'dec [deg]',
									'flux [nJy]',
									'flux error [nJy]'])
				df.to_csv(f'{bigsmall_id_fname}_extracted_sources.csv')

			#write fluxes to file
			np.savetxt(fluxfilename, np.array([true_flux, pred_flux]).T, fmt='%.10f', header = 'true_flux pred_flux')

			true_flux = np.array(true_flux)
			pred_flux = np.array(pred_flux)

			#write objects to file
			np.save(axesfilename, np.asarray([true_ellipse_params, pred_ellipse_params]))

		flux_ana = flux_deviation_analysis(true_flux, pred_flux, true_ellipse_params, pred_ellipse_params, run, use_ellipses, self.SEx.total_stats_minflux)
		flux_ana.plot_true_pred(fixlims = True)
		flux_ana.plot_true_error()
		# flux_ana.plot_pdf_cdf()
		flux_ana.statistics(run, total_stats)

if __name__ == '__main__':
	parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
	# parser.add_argument('--dataset', help='Path to the downloaded speed dataset.', default='')
	parser.add_argument('--run', help = 'the run to analyse.', default = 14)
	parser.add_argument('--n_images', help = 'number of images to analyse. Give none if you want the program to detect the number of images itself.', default = None)
	parser.add_argument('--plot_pred', help = 'whether to plot the predictions.', default = False)
	parser.add_argument('--use_ellipses', help = 'whether to use ellipses from the ground truth to extract fluxes from the prediction image.', default = False)
	args = parser.parse_args()


	if args.n_images == None:
		n_images = None
	else:
		n_images = int(args.n_images)


	IP = imagePlotter()
	# IP.analyse_n_images(int(args.run), n_images, args.use_ellipses == 'True')

	if args.plot_pred:
		#plot 20 images
		IP.plot_predictions(int(args.run), n_images = 20) #20
		#plot a selection of good looking images in a grid
		# IP.plot_selected_imgs(int(args.run))
