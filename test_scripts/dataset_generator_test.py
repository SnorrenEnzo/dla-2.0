# Code adapted from:
# https://stanford.edu/~shervine/blog/keras-how-to-generate-data-on-the-fly

import numpy as np
import keras
from astropy.io import fits

from keras.models import Sequential
from keras.layers import Conv2D, Conv2DTranspose, Flatten, Dense, Dropout, Activation
from keras.optimizers import Adam, RMSprop

'''
This file is kept for testing purposes
It only works for 1 filter

'''

class DataGenerator(keras.utils.Sequence):
	'''Generates data for keras '''

	def __init__(self, list_IDs, batch_size=10, dim=(100,100),n_channels=1,
				shuffle=True):
		self.dim = dim
		self.batch_size = batch_size
		self.list_IDs = list_IDs # image IDs
		self.n_channels = n_channels
		self.shuffle = shuffle
		self.on_epoch_end()

	def __len__(self):
		'''
		Denotes number of batches per epoch. Commonly set to 
		#samples/batch_size so that the model sees training examples at most once per epoch
		'''
		return int(np.floor(len(self.list_IDs) / self.batch_size))

	# When the batch corresponding to a given index is called,
	# the generator executes the __getitem__ method to generate it.	
	def __getitem__(self, index):
		'''Generate one batch of data'''
		
		# Generate indexes of the batch
		indexes = self.indexes[index*self.batch_size:(index+1)*self.batch_size]

		# Find list of IDs
		list_IDs_temp = [self.list_IDs[k] for k in indexes]

		# Generate data
		X, Y = self.__data_generation(list_IDs_temp)

		return X, Y

	def on_epoch_end(self):
		self.indexes = np.arange(len(self.list_IDs))
		if self.shuffle == True:
			np.random.shuffle(self.indexes)

	def __data_generation(self, list_IDs_temp):
		'''Generates data containing batch_size samples' # X : (n_samples, *dim, n_channels)'''
		# Initialization
		X = np.empty((self.batch_size, *self.dim)) # ,self.n_channels
		Y = np.empty((self.batch_size, *self.dim)) # ,1
 
		# Generate data
		for i, ID in enumerate(list_IDs_temp):
			# Filename for 'P' filter image with current image ID
			filenames = [path + f'{fil}_{1}_{filter_to_name[fil]}_sci_{ID}.fits' for fil in sorted(filters_used)]
			filename = filenames[0] # testing purposes
			# Store samples
			with fits.open(filename) as image:
				X[i,] = image[0].data[:100,:100] # temp cut at 100

			# Store labels
			filename = filename.replace('sci','perfect')
			with fits.open(filename) as image:
				Y[i,] = image[0].data[:100,:100] # temp cut at 100

		X = X.reshape((self.batch_size,*self.dim,1))
		Y = Y.reshape((self.batch_size,*self.dim,1))

		return X, Y

# Added a 1 for testing
path = '/disks/strw18/DLC_Team_Right_Model_data/Basic_4/1/'

# Temp
filter_to_name = {'P':'spitzer-mips24'}
filters_used = ['P']


params = {'dim': (100,100),
			'batch_size': 10,
			'n_channels': 1,
			'shuffle': True}
# Datasets
partition = {'train': np.arange(0,320),
			'validation': np.arange(320,420)}
# Generators
training_generator = DataGenerator(partition['train'], **params)
validation_generator = DataGenerator(partition['validation'], **params)

# Temporary parameters
num_filters = 1
n_conv_kernel = 6
ker_size = 5
learning_rate = 0.01

SRCNN = Sequential()

SRCNN.add(Conv2D(filters=n_conv_kernel, kernel_size=ker_size, padding='same',
				 kernel_initializer='glorot_uniform', activation='relu',
				 use_bias=True, input_shape=(100, 100, num_filters)))

SRCNN.add(Conv2D(filters=n_conv_kernel, kernel_size=5,padding='same'
	,kernel_initializer='glorot_uniform',activation='relu'
	,use_bias=True))

SRCNN.add(Conv2D(filters=1, kernel_size=5,padding='same'
	,kernel_initializer='glorot_uniform',activation='relu'
	,use_bias=True))

SRCNN.compile(optimizer = Adam(lr = learning_rate), loss = 'mean_squared_error')

print (SRCNN.summary())

# Train model on dataset
hist = SRCNN.fit_generator(generator=training_generator,
                    validation_data=validation_generator,
                    use_multiprocessing=True,
                    workers=6, epochs = 100)
