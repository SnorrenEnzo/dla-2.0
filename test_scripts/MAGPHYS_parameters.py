'''User inputs'''
#files & directories
input_catalog = '/net/amstelmeer/data3/MAGPHYS/example_catalog.fits'

#cosmology
H0 = 70.0 #km/s/mpc
omega_matter = 0.3
omega_lambda = 0.7

#catalog stuff
redshift = 'z' #columnname for column containing source redshifts
rows_to_fit = [5] #iterable containing the row numbers of the sources to fit (e.g. range(0,99999999))

#catalog photometry
flux_conversion = 1.0e-6 #all fluxes are multiplied by this number (e.g. to convert from uJy in the catalog to Jy used in the fitting by multiplying with 1.0e-6)
filter_fluxes = [] #columnname for column containing flux in jansky
filter_errors = [] #columnname for column containing error in jansky
filter_refnrs = [] #reference number in the magphys filters.log file
filter_wavelengths = [] #filter wavelength in micrometers
filter_bandwidths = [] #filter width in micrometers (optinal; used for plotting)

#filter
filter_fluxes.append('FLUX_subaru-B')
filter_errors.append('errFLUX_subaru-B')
filter_refnrs.append(101)
filter_wavelengths.append(0.44582763)
filter_bandwidths.append(0.0)

#filter
filter_fluxes.append('FLUX_subaru-V')
filter_errors.append('errFLUX_subaru-V')
filter_refnrs.append(102)
filter_wavelengths.append(0.5477747)
filter_bandwidths.append(0.0)

#filter
filter_fluxes.append('FLUX_subaru-i')
filter_errors.append('errFLUX_subaru-i')
filter_refnrs.append(103)
filter_wavelengths.append(0.76837015)
filter_bandwidths.append(0.0)

#filter
filter_fluxes.append('FLUX_vista-Y')
filter_errors.append('errFLUX_vista-Y')
filter_refnrs.append(104)
filter_wavelengths.append(1.0221548)
filter_bandwidths.append(0.0)

#filter
filter_fluxes.append('FLUX_vista-Ks')
filter_errors.append('errFLUX_vista-Ks')
filter_refnrs.append(105)
filter_wavelengths.append(2.1527567)
filter_bandwidths.append(0.0)

#filter
filter_fluxes.append('FLUX_spitzer-irac2')
filter_errors.append('errFLUX_spitzer-irac2')
filter_refnrs.append(106)
filter_wavelengths.append(4.522775)
filter_bandwidths.append(0.0)

#filter
filter_fluxes.append('FLUX_spitzer-irac4')
filter_errors.append('errFLUX_spitzer-irac4')
filter_refnrs.append(107)
filter_wavelengths.append(8.0119705)
filter_bandwidths.append(0.0)

#filter
filter_fluxes.append('FLUX_spitzer-mips24')
filter_errors.append('errFLUX_spitzer-mips24')
filter_refnrs.append(108)
filter_wavelengths.append(23.84971)
filter_bandwidths.append(0.0)

##filter
#filter_fluxes.append('')
#filter_errors.append('')
#filter_refnrs.append()
#filter_wavelengths.append()
#filter_bandwidths.append()

#magphys
deltaz = 0.0025 #MUST BE MULTIPLE OF 0.0025!!!!!!!!!!!   0.005 is good enough for most applications
overwrite_colors = True
overwrite_fits = True
overwrite_catalog = True
first_time_magphys = True #change this only if you are running a fresh download of magphys for the very first time
save_catalog_every_n_sources = 10

'''End user inputs'''


'''Default Settins'''
import os
NAME = os.path.basename(__file__) #not necessary to change
NAME = NAME[11:-3]
savedir = '/net/amstelmeer/data3/MAGPHYS/'+'%s/'%(NAME) #can be left like this
magphysdir = '/net/amstelmeer/data3/MAGPHYS/magphys_highz/' #does not have to be changed

'''Fixed inputs'''
colorsdir = savedir+'colors/'
plotsdir = savedir+'plots/'
datadir = savedir+'data/'

'''Sorting filters'''
filter_wavelengths,filter_fluxes,filter_errors,filter_refnrs,filter_bandwidths = (list(t) for t in zip(*sorted(zip(filter_wavelengths,filter_fluxes,filter_errors,filter_refnrs,filter_bandwidths)))) #sorting by effective wavelenth is required










