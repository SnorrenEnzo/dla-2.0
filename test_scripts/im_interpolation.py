import numpy as np
from scipy import ndimage
import matplotlib.pyplot as plt

from astropy.io import fits
from astropy.stats import sigma_clipped_stats

#plt.rc('text', usetex=True)

def test_interp_order():
	fig, axs = plt.subplots(2,4, figsize=(15,6))
	ax = axs.ravel()

	im0 = ax[0].imshow(hi_res)
	#cbar0 = plt.colorbar(im0, ax=ax[0])
	ax[0].set_title('High res, Total flux: {:.2e}'.format(hi_res.sum()))
	im1 = ax[1].imshow(low_res)
	#cbar1 = plt.colorbar(im1, ax=ax[1])
	ax[1].set_title('Low res, Total flux: {:.2e}'.format(low_res.sum()))

	for i in range(0,6):
		interp = ndimage.zoom(low_res, zoom=zoom, order=i)/zoom**2
		im = ax[2+i].imshow(interp)
		#cbar = plt.colorbar(im, ax=ax[2+i])
		ax[2+i].set_title('Order = {0}, Total flux: {1:.2e}'.format(i,interp.sum()))


path = '/disks/strw18/DLC_Team_Right_Model_data/NEW'
HR_images = np.zeros((100,200,200))
interp_images = np.zeros((100,200,200))

for i in range(100):

	field = np.random.randint(0,999)
	image = np.random.randint(0,99)

	low_res = fits.open(path+'/{0}/I_{0}{1:02d}_herschel-pacs100_sci.fits'.format(field,image))[0].data
	hi_res = fits.open(path+'/{0}/I_{0}{1:02d}_herschel-pacs100_sci_uniform.fits'.format(field,image))[0].data

	x = np.linspace(0,low_res.shape[0],low_res.shape[0])
	y = np.linspace(0,low_res.shape[1],low_res.shape[1])
	xx, yy = np.meshgrid(x,y)

	xNew = np.linspace(0,low_res.shape[0],hi_res.shape[0])
	yNew = np.linspace(0,low_res.shape[1],hi_res.shape[1])
	zoom = hi_res.shape[0]/low_res.shape[0]

	mean1, median1, std1 = sigma_clipped_stats(low_res, sigma=2.0)
	interp = low_res - median1/zoom

	interp = ndimage.zoom(interp, zoom=zoom, order=1)/zoom**2
	interp += np.random.normal(0, std1/zoom, size=interp.shape)

	
	fig, ax = plt.subplots(1,3, figsize=(10,5))

	ax[0].imshow(hi_res)
	ax[0].set_title('High res, Total flux: {:.2e}'.format(hi_res.sum()))
	ax[1].imshow(low_res)
	ax[1].set_title('Low res, Total flux: {:.2e}'.format(low_res.sum()))
	ax[2].imshow(interp)
	ax[2].set_title('Interpolation, Total flux: {:.2e}'.format(interp.sum()))
	plt.show()
	
	HR_images[i,:,:] = hi_res[:200,:200]
	interp_images[i,:,:] = interp[:200,:200]

plt.hist(HR_images.flatten(), range=(-6000,6000), color='r', bins=50, alpha=0.5, label='High res')
plt.hist(interp_images.flatten(), range=(-6000,6000), color='k', histtype='step', bins=50, alpha=0.5, label='Interpolation')
plt.legend()
plt.savefig('interpolation_hist.png')
plt.show()

