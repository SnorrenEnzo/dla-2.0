import numpy as np
import pandas as pd
from tqdm import tqdm
# from numdisplay_zscale import zscale
from PIL import Image
from astropy.io import fits
from astropy import wcs
from astropy.table import Table
from astropy.coordinates import SkyCoord
from astropy import units as u

import os

def main():
	
	path = '/disks/strw18/DLC_Team_Right_Model_data/NEW/1/'

	tot_FIR = []
	tot_im = []
	for i in range(1):
		im = f'I_{100+i}_herschel-pacs100_perfect_uniform.fits' 

		hdulist = fits.open(path+im)
		header = hdulist[0].header

		header['CDELT1'] = float(header['CDELT1'])
		header['CDELT2'] = float(header['CDELT2'])
		header['CRPIX1'] = float(header['CRPIX1'])
		header['CRPIX2'] = float(header['CRPIX2'])
		header['CRVAL1'] = float(header['CRVAL1'])
		header['CRVAL2'] = float(header['CRVAL2'])

		tabledata = Table.read(path+f'catalog_{100+i}.fits')

		x, y, flux = run_sep(hdulist[0].data.byteswap(inplace=True).newbyteorder(), perfect=True)
		#plot_sex_perf(hdulist[0].data.byteswap(inplace=True).newbyteorder(), 1)

		w = wcs.WCS(hdulist[0].header)
		ra_s, dec_s = w.wcs_pix2world(x,y,0)

		c = SkyCoord(ra = ra_s*u.degree, dec= dec_s*u.degree)
		catalog = SkyCoord(ra = tabledata['RA'].T*u.degree, dec= tabledata['DEC'].T*u.degree)
		idx, d2d, d3d = c.match_to_catalog_sky(catalog)
		#idx gives index of catalog matches

		#print(tabledata['FLUX'][0,idx,-1].T, flux*1e-3)



if __name__ == '__main__':
	main()
