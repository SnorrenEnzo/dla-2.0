import numpy as np 
import glob
import os

from astropy.table import Table
import astropy

def main():
	path = '/disks/strw18/DLC_Team_Right_Model_data/NEW/'

	for folder in glob.glob(path+'*'):
		for index in glob.glob(folder+'/catalog_*.fits'):
			if 'fixed' in index:
				continue

			if os.path.isfile(index[:-5]+'_fixed.fits'):
				continue

			print(f'Fixing catalog {index}..')

			try:
				read_catalog = Table.read(index)

				table_fixed = Table()
				for column in read_catalog.colnames[:-8]:
					table_fixed[column] = read_catalog[column].T

				for column in ['FLUX', 'FLUX_DISK', 'FLUX_BULGE']:
					for i, band in enumerate(read_catalog['BANDS'].T):

						band = band[0].decode("utf-8")
						table_fixed[column+'_'+band.strip()] = read_catalog[column][0,:,i].T
						table_fixed['err'+column+'_'+band.strip()] = read_catalog[column][0,:,i].T/1e3

				table_fixed.write(index[:-5]+'_fixed.fits', format='fits', overwrite=True)

			except ValueError:
				print('Cannot load catalog for some reason, moving on')
				continue

			except astropy.io.registry.IORegistryError:
				print('Cannot load catalog for some reason, moving on')
				continue


if __name__ == '__main__':
 	main() 